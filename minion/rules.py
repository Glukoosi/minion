import hashlib
import pathlib
import re
import subprocess
from logging import Logger, INFO
from typing import List, Dict, Tuple, Set, Optional, Union, Any

import psutil

from minion import predicate
from minion.interfaces import DataFile, Rule, FileMetadata, RuleOption
from minion.predicate import RuleReference, PredicateValues, PredicateValue
from minion.value import ValueName, LOCAL


class CommandPart:
    def __init__(self, variable: str = None):
        self.variable = variable

    def is_pipe(self, original: str) -> Optional[bool]:
        return original.lstrip().startswith('|')

    def apply(self, original: str, values: Dict[str, PredicateValue]) -> str:
        return original

    def apply_pipe(self, original: str) -> List[str]:
        if self.is_pipe(original):
            return [original.lstrip()[1:].lstrip()]
        return [original]

    def variables(self) -> Set[str]:
        return set()

    def __repr__(self):
        return ''


class FileNameVariable(CommandPart):
    def __init__(self, variable: str, can_be_value=False):
        super().__init__(variable)
        self.can_be_value = can_be_value

    def is_pipe(self, original: str) -> Optional[bool]:
        return None

    def apply(self, original: str, values: Dict[str, PredicateValue]) -> str:
        value = values.get(self.variable)
        if value is None:
            return ""
        if not value.files:
            if not self.can_be_value:
                raise Exception(f"'{self.variable}' is not a file")
            return value.value()
        return " ".join([f.as_string() for f in value.files])

    def apply_pipe(self, original: str) -> List[str]:
        return ['', self.variable]

    def variables(self) -> Set[str]:
        return {self.variable}

    def __repr__(self):
        return f"$file({self.variable})"


class FileValueVariable(CommandPart):
    def __init__(self, variable: str):
        super().__init__(variable)

    def is_pipe(self, original: str) -> Optional[bool]:
        return None

    def apply(self, original: str, values: Dict[str, PredicateValue]) -> str:
        value = values.get(self.variable)
        if value is None:
            return ""
        return value.value()

    def apply_pipe(self, original: str) -> List[str]:
        return [original]

    def variables(self) -> Set[str]:
        return {self.variable}

    def __repr__(self):
        return f"$value({self.variable})"


class CommandRule(Rule):
    def __init__(self, start: ValueName, end: ValueName):
        super().__init__(start, end)
        self.commands: List[str] = []
        self.parts: List[List[Tuple[str, CommandPart]]] = []
        self.pattern = re.compile(
            r'(\$\$?[a-zA-Z_@<][a-zA-Z0-9_]*\([^()$]+\))|' +
            r'(\$\$?[a-zA-Z_@<][a-zA-Z0-9_]*)|' +
            r'(\$\$?\([^()$]+\))'
            )

    @classmethod
    def new(cls, start: Union[str, ValueName], end: Union[str, ValueName],
            predicate: Optional[predicate.PredicateOperation] = None, commands: List[str] = None,
            options: Dict[RuleOption, Any] = None) -> 'CommandRule':
        c = CommandRule(ValueName.of(start), ValueName.of(end))
        c.predicate = predicate
        c.commands = commands or []
        c.options = options or {}
        c.finish()
        return c

    def finish(self) -> Rule:
        # all used variables must be used in conditions, otherwise it gets too messy if
        # variables used in commands
        ref_list = list(self.predicate.list_variables()) if self.predicate else []

        # create variable -> value map
        self.variables[self.start.name] = RuleReference(self.start)
        for v in ref_list:
            self.variables[v.name.name] = v
        for v in self.local_variables:
            self.variables[v.name] = RuleReference(v, scope=LOCAL)

        # parse command parts
        self.parts = [self.__parse_command(c) for c in self.commands]
        # finally, the digest
        hash = hashlib.sha256()
        hash.update(self.__repr__().encode('utf8'))
        self.digest = hash.hexdigest()[0:16]
        return self

    def alias(self) -> bool:
        return not self.commands

    def create_value_map(self, out_file: DataFile, predicate: PredicateValues) -> Dict[str, PredicateValue]:
        replace = {
            'OUT': PredicateValue(files=[out_file]),
            '@': PredicateValue(files=[out_file]),
        }
        for p in predicate.iterate():
            replace[p.name] = p
        for name, ref in self.variables.items():
            # rule renaming changes some variables, reflect it (rule == None only in unit tests)
            if ref.name.name in replace and ref.name.name != name:
                replace[name] = replace[ref.name.name]
                del replace[ref.name.name]
        return replace

    def __parse_command(self, command: str) -> List[Tuple[str, CommandPart]]:
        variables = set(self.variables.keys()).copy()
        variables.add('OUT')
        variables.add('@')
        variables.add('IN')
        variables.add('<')

        c_str = self.pattern.split(command)
        c_str = list(filter(lambda s: s, c_str))
        parts = []
        for s in c_str:
            part = None
            v = s
            postfix = ''
            if s.startswith('$$'):
                v = s[1:]  # $$ -> $
            elif s.startswith('$(') and s.endswith(')'):
                v_name = s[2:-1]
                if v_name in variables:
                    part = FileNameVariable(v_name, can_be_value=True)  # $(file)
            elif s.startswith('$'):
                inner = s[1:]
                if inner in variables:
                    part = FileNameVariable(inner, can_be_value=True)  # $filename
                if '(' in inner and inner.endswith(')'):
                    i = inner.index('(')
                    name = inner[0:i]
                    v_name = inner[i + 1:-1]
                    if name in variables:
                        part = FileNameVariable(name)  # $filename(leave)
                        v = name
                        postfix = f"({v_name})"
                    elif name == 'file':
                        part = FileNameVariable(v_name)  # $file(filename)
                    elif name == 'value':
                        part = FileValueVariable(v_name)  # $value(filename)
            if part is None:
                part = CommandPart()
            parts.append((v, part))
            if postfix:
                parts.append((postfix, CommandPart()))
        return parts

    def get_command_variables(self) -> Set[str]:
        var_set = set()
        for part in self.parts:
            for _, c in part:
                var_set.update(c.variables())
        return var_set

    def replace(self, command: List[Tuple[str, CommandPart]],
                value_map: Dict[str, PredicateValue]) -> Tuple[List[str], str]:
        # push some data through stdin?
        stdin_to = 0
        for s, cmd in command:
            pipe_stdin = cmd.is_pipe(s)
            if pipe_stdin is None:
                stdin_to += 1
                continue
            if not pipe_stdin:
                stdin_to = 0  # not after all
            break
        else:
            stdin_to = -1  # not

        stdin_vars = []
        n_command = []
        i = 0
        for s, cmd in command:
            if stdin_to >= 0 and i <= stdin_to:
                # collect piped variables
                pipe = cmd.apply_pipe(s)
                n_command.append(pipe[0])
                stdin_vars.extend(pipe[1:])
            else:
                n_command.append(cmd.apply(s, value_map))
            i += 1
        return stdin_vars, ''.join(n_command)

    def apply(self, predicate: PredicateValues, out_file: DataFile, meta: FileMetadata, timeout: float,
              logger: Logger):
        var_set = self.get_command_variables()
        out_used = var_set.intersection({'OUT', '@'})

        work_dir = out_file.root.resolve()
        value_map = self.create_value_map(out_file, predicate)
        exit_code = 0
        for part_i, part in enumerate(self.parts):
            stdin_vars, command = self.replace(part, value_map)
            print_command = command

            stdin = None
            stdin_files: List[DataFile] = []
            if stdin_vars:
                stdin = subprocess.PIPE
                for var in stdin_vars:
                    var_p = predicate.get(var)
                    stdin_files.extend((var_p.files or []) if var_p else [])
                print_files = ' '.join([f.as_string() for f in stdin_files[:25]])
                files_cut = ' ...' if len(stdin_files) > 25 else ''
                print_command = f"cat {print_files}{files_cut} | " + print_command

            stdout = subprocess.PIPE
            if part_i == len(self.parts) - 1 and not out_used:
                stdout = open(out_file.path, 'wb')
                print_command = print_command + f" > {out_file.as_string()}"

            meta.log.append(('in', print_command))
            logger.info(print_command)
            p = None
            try:
                p = subprocess.Popen(command, stdin=stdin, stdout=stdout, stderr=subprocess.PIPE,
                                     shell=True, cwd=work_dir.as_posix())
                exit_code = -1
                # write all stdin output
                buf_size = 1024  # * 1024
                for file in stdin_files:
                    with file.path.open('rb') as f:
                        buf = f.read(buf_size)
                        while buf:
                            if p.poll():
                                break
                            p.stdin.write(buf)
                            buf = f.read(buf_size)
                    if p.poll():
                        break
                # we assume that large data sets are not dumped to stdout or stderr, but either piped
                # through or handled in files
                stdout_data, stderr_data = p.communicate(timeout=timeout)
                exit_code = p.returncode
                if stderr_data:
                    self.__write_to_log(stderr_data, logger)
                    txt = stderr_data.decode('ascii', errors='ignore').strip()
                    for line in txt.split("\n"):
                        meta.log.append(('err', line))
                if stdout_data:
                    self.__write_to_log(stdout_data, logger)
                    txt = stdout_data.decode('ascii', errors='ignore').strip()
                    for line in txt.split("\n"):
                        meta.log.append(('out', line))
            except subprocess.TimeoutExpired:
                logger.warning("Timeout of %0.1fs fired", timeout)
                # must use psutil to kill the shell and its subprocesses...
                proc = psutil.Process(p.pid)
                for p in [proc] + proc.children(recursive=True):
                    logger.debug("killing %d", p.pid)
                    p.kill()
                # man page of 'timeout' command
                # If the command times out ... then exit with status 124
                exit_code = 124
            finally:
                if stdout and stdout != subprocess.PIPE:
                    stdout.close()
            if exit_code != 0:
                break
        meta.exit_code = exit_code

    @classmethod
    def __write_to_log(cls, data: bytes, logger: Logger):
        if not logger.isEnabledFor(INFO):
            return
        for s in data.decode('ascii', errors='backslashreplace').split('\n'):
            logger.info(s)

    def to_string(self) -> str:
        if self.input_file:
            key_w = 'file'
            start = self.input_file.as_string()
        else:
            key_w = 'rule'
            start = self.start.name
        predicate = ''
        if self.predicate:
            predicate = ' with ' + self.predicate.__str__()
        options = ''
        if self.is_set(RuleOption.BATCH):
            options += ' --batch'
        if self.is_set(RuleOption.TARGET):
            options += ' --target'
        commands = ''
        if self.commands:
            commands = '\n\t' + '\n\t'.join(self.commands)
        return f"{key_w} {start}{predicate} => {self.end}{options}{commands}"

    def __repr__(self) -> str:
        digest = ''
        if self.digest:
            digest = f"# {self.digest}\n"
        return f"{digest}{self.to_string()}"


def default_search_path(*with_paths: pathlib.Path) -> List[pathlib.Path]:
    return [pathlib.Path(), (pathlib.Path(__file__).parent / 'rules').resolve()] + list(with_paths)


def locate(rule_file: str, rule_search_paths: List[pathlib.Path] = None) -> pathlib.Path:
    sp = default_search_path() if rule_search_paths is None else rule_search_paths
    for search_path in sp:
        path = search_path / rule_file
        if path.is_file():
            return path
    paths = " ".join([p.as_posix() for p in sp])
    raise Exception(f"Could not locate '{rule_file}' from paths: {paths}")
