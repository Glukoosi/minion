import logging
import sys
import time
import traceback
from collections import deque
from logging import Logger
from typing import List, Deque, Dict, Optional, Callable, Iterable, Set, Tuple

from minion import predicate
from minion.context import FileContext, FileState, Links
from minion.file import DataFile
from minion.file_manager import SubDirectoryFileManager
from minion.interfaces import FileManager, Rule, FileMetadata, RuleOption
from minion.predicate import PredicateValues, PredicateValue, RuleReference
from minion.value import ValueName, GLOBAL, INPUT_VALUE


class PredicateMappings(predicate.PredicateValues):
    def __init__(self, logger: Logger, rule: Rule, values: Dict[str, List[DataFile]],
                 defined_values: Dict[str, PredicateValue]):
        super().__init__(logger, rule.start.name)
        self.rule = rule
        self.rule_mappings = {n: r.name.name for n, r in rule.variables.items()}
        self.values = values
        self.defined_values = defined_values
        self.known_values: Dict[str, predicate.PredicateValue] = {}

    def iterate(self) -> Iterable[predicate.PredicateValue]:
        return [self.get(v) for v in self.rule.variables.keys()]

    def get(self, value_name: str) -> Optional[predicate.PredicateValue]:
        v_name = self.base_name + value_name
        if v_name in self.known_values:
            return self.known_values[v_name]
        if v_name in self.defined_values:
            value = self.defined_values[v_name]
        else:
            m_name = self.rule_mappings.get(v_name, v_name)
            files = self.values.get(m_name)
            value = predicate.PredicateValue(v_name, files=files)
        self.known_values[v_name] = value
        return value

    def update(self, value_name: str, value: Optional[predicate.PredicateValue]):
        v_name = self.base_name + value_name
        self.known_values[v_name] = value


class ContextListener:
    def check_context(self, context: 'PreparedContext'):
        pass

    def start_context(self, context: 'PreparedContext'):
        pass

    def end_context(self, context: 'PreparedContext'):
        pass


class PreparedContext:
    def __init__(self, context: FileContext, rules: Iterable[Rule]):
        self.context = context
        self.manager = context.manager
        self.logger = self.manager.logger
        self.rules = deque([context.context_rule] if context.context_rule else rules)
        self.expand_to_files: List[DataFile] = []
        self.log_name = context.file.as_string()
        self.batch_root = False

    def name(self) -> ValueName:
        return self.context.name

    def path_string(self) -> str:
        return self.context.file.as_string()

    def parent(self) -> Optional[FileContext]:
        return self.context.parent

    def peek_rule(self) -> Optional[Rule]:
        return self.rules[0] if self.rules else None

    def is_ready_to_apply(self):
        # FIXME: Create method which just checks if ready!
        for ref, scope in self.context.requirements.items():
            for ctx in scope.get_value(ref, scope=scope):
                if not self.batch_root and ctx.state != FileState.READY:
                    return False
                elif self.batch_root and not ctx.state.is_tested_or_ready():
                    return False
        return True

    def run(self, listeners: List[ContextListener]):
        for cl in listeners:
            cl.check_context(self)
        self.context.start_time = time.time()
        self.apply(listeners)
        for cl in listeners:
            cl.end_context(self)
        self.context.elapsed_time = time.time() - self.context.start_time
        elapsed_time = self.context.elapsed_time * 1000
        self.logger.debug("%s took %0.0f ms", self.log_name, elapsed_time)

    def __resolve_input_files(self, rule: Rule,
                              metadata: Optional[FileMetadata]) -> Tuple[bool, Dict[str, List[DataFile]]]:

        changes, in_files = self.context.resolve_input_files(rule, metadata)
        # debug log all input files
        if self.logger.isEnabledFor(logging.DEBUG):
            for ref, i_files in in_files.items():
                if not i_files:
                    self.logger.debug("%s = ()", ref)
                for i_f in i_files:
                    self.logger.debug("%s = %s", ref, i_f.as_string())

        return changes, in_files

    def __already_applied(self, rule: Rule, input_changes: bool, in_files: Dict[str, List[DataFile]],
                          metadata: FileMetadata) -> bool:
        if not metadata:
            # file not created by us, do not touch
            self.logger.info("[?] %s", self.log_name)
            # self.context.error = FileExistsError(f"File {self.context.file} created by someone else")
            self.context.state = FileState.DROPPED
            return True
        if input_changes:
            # NOTE: We do not check for input file changes, as those are already detected from FileContexts
            # and reported here
            return False
        if metadata.exit_code != 0:
            # failed, try again
            return False
        if not self.manager.ignore_changes:
            # check for ignore changes in rules or source files
            if metadata.rule_digest != rule.digest or metadata.rule.start != rule.start or metadata.rule.end != rule.end:
                return False
        # same rule, applied to current version of the file - nothing to do
        self.logger.info(f"[/] %s", self.log_name)
        if self.context.file.is_dir():
            # output is a directory, use metadata to figure out which files were created by this rule
            root_file = self.manager.root_file
            self.context.directory_of = [root_file / of.path for of in metadata.output]
        return True

    def __do_predicate(self, rule: Rule, in_files: Dict[str, List[DataFile]]) -> Tuple[bool, PredicateValues]:
        cond_values = PredicateMappings(self.logger, rule, in_files, self.manager.registry.defined_values)
        check_pass = rule.predicate.evaluate(cond_values).bool_value() if rule.predicate else True
        if not check_pass:
            if not self.rules:
                # predicate failed and no other rules to apply...
                self.logger.debug(f"[ ] %s", self.log_name)
                self.context.state = FileState.DROPPED
            return False, cond_values
        return True, cond_values

    def __process_output(self, rule: Rule):
        file = self.context.file
        if self.batch_root and rule.is_set(RuleOption.BATCH):
            # relocate interim files to final positions
            for ref, scope in self.context.requirements.items():
                for out in scope.get_value(ref):
                    if out.state != FileState.TESTED:
                        continue
                    in_file = out.parent.file
                    interim_file = file / in_file.as_string()
                    out_file = out.file
                    self.logger.info("mv %s -> %s", interim_file.as_string(), out_file.as_string())
                    if not interim_file.exists():
                        error = f"Rule {rule.__str__()} did not produce interim file {interim_file}"
                        sys.stderr.write(error + '\n')
                        self.__drop_all_output()
                        self.context.error = Exception(error)
                        return
                    self.manager.registry.relocate_rule(in_file, interim_file, out_file, rule)
        elif file.is_dir() and any(self.manager.rules.list_reachable_values(rule.end)):
            # list files, if potentially manipulated by rules
            self.context.directory_of = [f for f in file.list_files(recurse=False) if self.manager.is_data_file(f)]

    def __drop_all_output(self):
        for ref, scope in self.context.requirements.items():
            for out in scope.get_value(ref):
                if out.state == FileState.TESTED:
                    out.state = FileState.DROPPED

    def __apply_rule(self, rule: Rule, predicate: PredicateValues,
                     metadata: FileMetadata) -> Optional[FileMetadata]:
        context = self.context
        try:
            metadata = self.manager.apply_rule(predicate, context.file, rule, self.logger)
        except Exception as e:
            metadata = None
            context.error = e
            context.state = FileState.DROPPED
            if self.logger.isEnabledFor(logging.DEBUG):
                traceback.print_exc()
            else:
                self.logger.error(str(e))
        return metadata

    def apply(self, listeners: List[ContextListener] = None, capture_errors=True) -> 'PreparedContext':
        self.logger.debug("--- %s", self.log_name)
        context = self.context

        rule = self.rules.popleft()
        if rule.is_set(RuleOption.DIRECTORY) != (context.parent and context.parent.is_dir()) and not self.batch_root:
            # cannot process a directory
            self.logger.debug("[ ] %s (dir <-> file)", self.log_name)
            context.state = FileState.DROPPED
            return self
        context.batch_root = self.batch_root
        context.state = FileState.EVALUATE
        file = context.file
        file_exists = file.exists()
        metadata = self.manager.registry.get_metadata(file, full_data=False) if file_exists else None

        input_changes, in_files = self.__resolve_input_files(rule, metadata)

        check_pass, predicate = self.__do_predicate(rule, in_files)
        if not self.batch_root and not check_pass:
            self._call(listeners)
            return self  # not applied
        context.context_rule = rule
        self.rules.clear()
        context.state = FileState.TESTED

        if not self.batch_root and rule.pack_files():
            if rule.is_set(RuleOption.BATCH):
                self.logger.info("[_] %s", self.log_name)
            else:
                self.logger.debug("[_] %s", self.log_name)
            context.created = input_changes
            self._call(listeners)
            return self  # leave data waiting for the centralized processing
        context.state = FileState.EVALUATE

        if rule.alias():
            # no processing, just an alias
            if context.parent.parent and context.parent.parent.file == context.parent.file:
                # ...break the loop
                self.logger.debug("break alias loop: %s", self.log_name)
                context.state = FileState.DROPPED
                self._call(listeners)
                return self
            self.logger.debug("[A] %s", self.log_name)
            context.make_into_alias()
            self.__process_output(rule)
            self._call(listeners)
            return self

        if file_exists:
            # check if the file we have will do
            if self.__already_applied(rule, input_changes, in_files, metadata):
                # do not run the rule again
                self._call(listeners)
                return self

        self.logger.info("[x] %s", self.log_name)
        context.created = True
        metadata = self.__apply_rule(rule, predicate, metadata)
        if not metadata or metadata.exit_code != 0:
            self.logger.info("[E] %s", self.log_name)
        if metadata and metadata.exit_code != 0:
            source = f" ({rule.source[0]} line {rule.source[1]})" if rule.source else ""
            context.error = Exception(f"Exit code {metadata.exit_code} for {rule.__str__()}{source}")
            self.logger.error(context.error)
            context.state = FileState.DROPPED
        if context.error and not capture_errors:
            self._call(listeners)
            raise context.error

        self.__process_output(rule)
        self._call(listeners)
        return self

    def _call(self, listeners: List[ContextListener]):
        if listeners:
            # call listeners, may block (breakpoint)
            for cl in listeners:
                cl.start_context(self)


    def __repr__(self):
        return f"{self.context}"


class Builder:
    def __init__(self, manager: FileManager, sources: List[DataFile]):
        self.manager = manager
        self.registry = manager.registry
        self.root_file = manager.root_file
        self.global_context = FileContext(self.manager, self.root_file, GLOBAL, state=FileState.READY)
        self.contexts: List[FileContext] = []
        self.prepared: Deque[PreparedContext] = deque()
        self.needed_requirements: Dict[ValueName, Set[PreparedContext]] = {}
        for file in sources:
            if not self.manager.is_data_file(file):
                self.manager.logger.warning("skip %s", file.as_string())
                continue  # exclude
            self.manager.logger.debug("source %s", file.as_string())
            ctx = self.global_context.add_output(INPUT_VALUE, file=file, append=True)
            ctx.state = FileState.READY
            ctx.possible_values = manager.rules.list_reachable_values(INPUT_VALUE)
            ctx.is_directory = file.is_dir()
            self.global_context.possible_values.update(ctx.possible_values)
            self.contexts.append(ctx)
        self.debug_targets: Set[ValueName] = set()
        self.batch_contexts: Dict[Tuple[FileContext, ValueName], FileContext] = {}

    @classmethod
    def new(cls, root: DataFile, rules: List[Rule] = None, rule_file: Optional[DataFile] = None,
            sources: List[DataFile] = None) -> 'Builder':
        manager = SubDirectoryFileManager.new(root, rules=rules, rule_file=rule_file)
        return Builder(manager, sources or [])

    def prepare(self, context: FileContext,
                batch_rule: Rule = None, batch_scope: Optional[FileContext] = None) -> Optional[PreparedContext]:
        if context.state != FileState.IDLE:
            return None
        context.state = FileState.PREPARED
        parent = context.parent
        if batch_rule:
            rules = [batch_rule]
        elif parent:
            rules = list(parent.get_rules_for(context.name))
        else:
            rules = []
        evo = PreparedContext(context, rules)
        evo.batch_root = batch_rule is not None
        self.__add_to_prepared(evo)
        if rules:
            rule = evo.peek_rule()
            if rule.end in self.debug_targets:
                # enable full logging for this context!
                evo.logger = logging.getLogger(rule.digest)
                evo.logger.setLevel(logging.DEBUG)
            self.__update_requirements(evo, rule, batch_scope)
            if not evo.batch_root and rule.pack_files():
                batch_scope = context.resolve_parent(rule.get_option(RuleOption.SCOPE) or GLOBAL)
                assert batch_scope
                batch_key = (batch_scope, context.name)
                if batch_key not in self.batch_contexts:
                    # first context of a batch processing, prepare the process context
                    batch_ctx = batch_scope.init_batch_rule(rule)
                    self.batch_contexts[batch_key] = batch_ctx
                    self.prepare(batch_ctx, batch_rule=rule, batch_scope=batch_scope)
        return evo

    def __update_requirements(self, prepared: PreparedContext, rule: Rule, batch_scope: FileContext = None):
        context = prepared.context
        context.update_requirements(rule, batch_scope)
        self.__insert_requirements_from(prepared)
        # ensure all requirements present or scheduled to be resolved
        for ref, scope in context.requirements.items():
            for req in scope.get_value(ref, scope=scope):
                self.prepare(req)

    def __add_to_prepared(self, context: PreparedContext):
        if context.rules and context.peek_rule().is_dependent():
            # rule with dependencies, schedule later
            self.prepared.append(context)
        else:
            # independent rule, can be evaluated right away - do it
            self.prepared.insert(0, context)

    def prepare_new(self, links: Iterable[FileContext]):
        for ctx in links:
            self.prepare(ctx)

    def prepare_outputs(self, context: FileContext, values: Set[ValueName]):
        created = context.create_missing_outputs(sorted(values))
        for ctx in created:
            self.prepare(ctx)

    def get_value(self, context: FileContext, name: ValueName) -> List[FileContext]:
        created = context.get_value(RuleReference(name))
        for ctx in created:
            self.prepare(ctx)
        return created

    def get_to_evaluate(self) -> Optional[PreparedContext]:
        count = 0
        while count < len(self.prepared):
            pre = self.prepared.popleft()
            count += 1
            if pre.is_ready_to_apply():
                return pre
            self.prepared.append(pre)
        return None

    def try_next_rule(self, prepared: PreparedContext) -> 'PreparedContext':
        self.__add_to_prepared(prepared)
        self.__update_requirements(prepared, prepared.peek_rule())
        return prepared

    def post_process(self, ready: PreparedContext) -> List[FileContext]:
        self.__withdraw_requirements_from(ready)
        context = ready.context
        if context.error:
            self.manager.logger.debug("error %s: %s", context.file.as_string(), str(context.error))
            context.state = FileState.DROPPED
            return [context]
        self.manager.logger.debug("done %s", context.file.as_string())
        if context.state == FileState.DROPPED:
            # excluded
            return [context]
        if ready.peek_rule():
            # was not ready after all... but can try next rule
            self.try_next_rule(ready)
            return []
        if ready.context.state == FileState.TESTED:
            # just tested... leave hanging
            return []
        required_outputs = self.__required_values(context)
        prep = context.post_process(required_outputs)
        context.state = FileState.READY
        self.prepare_new(prep)

        if ready.batch_root and not context.context_rule.is_set(RuleOption.BATCH):
            # value created through start symbols, but value location is the scope
            self.global_context.outputs.links[ready.name()] = Links(context)

            for ref, scope in context.requirements.items():
                for out in scope.get_value(ref):
                    if out.state != FileState.TESTED:
                        continue
                    out.state = FileState.DROPPED

        if ready.batch_root and context.context_rule.is_set(RuleOption.BATCH):
            r = []
            for ref, scope in context.requirements.items():
                for out in scope.get_value(ref):
                    if out.state != FileState.TESTED:
                        continue
                    out_prep = out.post_process(required_outputs)
                    out.state = FileState.READY
                    out.created = context.created
                    r.append(out)
                    self.prepare_new(out_prep)
            return r
        elif context.directory_of is not None:
            # create separate output for all output files of the directory
            exp_links = list(context.expand_to_files().iterate_contexts())
            for exp in exp_links:
                exp_prep = exp.post_process(required_outputs)
                self.prepare_new(exp_prep)
            exp_links.append(context)
            return exp_links
        return [context]

    def __required_values(self, from_context: FileContext) -> List[ValueName]:
        # NOTE: Unoptimally does not care which context we provide -> may be asking for something
        # which would only be required in a other context
        return list(self.needed_requirements.keys())

    def __insert_requirements_from(self, prepared: PreparedContext):
        context = prepared.context
        for ref in context.requirements.keys():
            for v in ref.all_names():
                wait_set = self.needed_requirements.setdefault(v, set())
                wait_set.add(prepared)

    def __withdraw_requirements_from(self, prepared: PreparedContext):
        context = prepared.context
        for ref in context.requirements.keys():
            for v in ref.all_names():
                assert v in self.needed_requirements, f"{v} for {prepared} not in requirements"
                wait_set = self.needed_requirements[v]
                assert prepared in wait_set, f"{prepared} not in wait set for {v}"
                wait_set.remove(prepared)
                if not wait_set:
                    del self.needed_requirements[v]

    def check_for_deadlock(self):
        if not self.prepared:
            return   # no deadlock
        context = self.prepared[0].context
        covered = set()
        loop = []
        while context is not None and len(loop) < 10:
            loop.append(context)
            pending = context.find_pending_requirement()
            context = pending
        deps = "\n  <- ".join([f.file.as_string() for f in loop])
        s = f"Deadlock:\n  {deps}"
        raise Exception(s)

    def __truediv__(self, value_name: str) -> Optional[FileContext]:
        for c in self.contexts:
            if c.file.name() == value_name:
                return c
        return None
