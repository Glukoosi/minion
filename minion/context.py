import itertools
import uuid
from enum import Enum
from typing import Dict, List, Optional, Iterable, Set, Tuple

from minion.interfaces import Rule, DataFile, FileManager, RuleOption, FileMetadata
from minion.predicate import RuleReference
from minion.value import ValueName, EXPAND_FILES, INPUT_VALUE, GLOBAL, INPUT_NAME, LOCAL


class FileState(Enum):
    IDLE = "idle"          # context idle, not for evaluation
    PREPARED = "prepared"  # context prepared and scheduled
    EVALUATE = "evaluate"  # evaluating or testing the command
    TESTED = "tested"      # tested
    READY = "ready"        # ready, (tested and) evaluated
    DROPPED = "dropped"    # context dropped, file not created

    def is_finished(self) -> bool:
        return self == self.READY or self == self.DROPPED

    def is_tested_or_ready(self) -> bool:
        return self == self.TESTED or self == self.READY

    def __str__(self):
        return self.value.__str__()


FILE_STATE_ORDER = {
    FileState.IDLE: 0,
    FileState.PREPARED: 1,
    FileState.EVALUATE: 2,
    FileState.TESTED: 3,
    FileState.READY: 4,
    FileState.DROPPED: 5,
}


class FileContext:
    def __init__(self, manager: FileManager, file: DataFile, name: ValueName, parent: Optional['FileContext'] = None,
                 state: FileState = FileState.IDLE):
        self.id = uuid.uuid4()
        self.manager = manager
        self.file = file
        self.name = name
        self.parent = parent
        self.state = state

        # when PREPARED
        self.context_rule: Optional[Rule] = None  # may be known when prepared or later if a choice
        self.batch_root = False  # the root context for a batch rule?

        # required values and their scopes
        self.requirements: Dict[RuleReference, FileContext] = {}

        # when READY
        self.start_time: Optional[float] = None
        self.created = False
        self.error: Optional[Exception] = None
        self.directory_of: Optional[List[DataFile]] = None
        self.possible_values: Set[ValueName] = set()
        self.elapsed_time = 0
        self.value_cache: Dict[ValueName, List[FileContext]] = {}
        # output links
        self.outputs = ContextLinks()

        if self.global_context():
            self.possible_values.add(INPUT_VALUE)

    def init_batch_rule(self, rule: Rule) -> 'FileContext':
        # FIXME: check that not multiple batch rules with same end
        if rule.is_set(RuleOption.BATCH):
            # special working directory for batch processing
            output_path = self.manager.registry.get_batch_dir() / rule.end.name
            b_file = self.manager.registry.get_file_by_path(output_path.as_posix())
        else:
            # the normal location
            b_file = self.__get_rule_output_file(rule)
        # create IDLE output context to be processed in batch by collected values
        b_context = FileContext(self.manager, b_file, rule.end)
        b_context.context_rule = rule
        # do not link as output, will be done after applied
        # self.outputs.links[rule.end] = Links(b_context)
        return b_context

    def make_into_alias(self) -> 'FileContext':
        if self.context_rule.input_file:
            self.file = self.context_rule.input_file
        else:
            self.file = self.parent.file
        return self

    def is_dir(self) -> bool:
        return not self.global_context() and self.file.is_dir()

    def is_excluded(self):
        return self.state == FileState.DROPPED and not self.error

    def add_output(self, name: ValueName, file: DataFile = None, append=False) -> 'FileContext':
        out_f = file
        if not out_f:
            if self.global_context():
                out_f = self.manager.root_file / name.name
            else:
                out_f = self.manager.get_target(self.file, name.name)
                assert out_f, f"No target for {self.file}..{name}"
        ctx = FileContext(self.manager, out_f, name, parent=self)
        # Note, requirements updated by builder!
        if append and name in self.outputs.links:
            self.outputs[name].append(ctx)
        else:
            assert name not in self.outputs, f"Adding new output {name}, but output already exists in {self}"
            self.outputs[name] = Links(ctx)
        return ctx

    def outputs_names_to_find(self, values: Set[ValueName]) -> List[ValueName]:
        outputs = set()
        for v in values:
            outputs.update(self.get_immediate_to_resolve(v))
        if EXPAND_FILES in self.outputs:
            outputs.add(EXPAND_FILES)
        return sorted(outputs)

    def update_requirements(self, for_rule: Rule, batch_scope: 'FileContext'):
        reqs = {}
        if batch_scope:
            # wait for components to be available
            reqs[RuleReference(for_rule.end)] = batch_scope
        for ref in for_rule.variables.values():
            if ref.scope == LOCAL:
                continue  # locally resolved - FIXME: Could start symbol be?
            if ref.name == for_rule.start:
                # rule start symbol is always there when rule is go, do not resolve here
                continue
            if ref.name.name in self.manager.registry.defined_values:
                continue  # defined value
            if batch_scope:
                if ref.name == INPUT_NAME and not self.manager.rules.get_rules(INPUT_NAME):
                    continue  # input file name
                scope = batch_scope
            else:
                scope = self.resolve_scope(ref.scope or ref.head())
                if scope is None:
                    if ref.name == INPUT_NAME:
                        # this is implicitly defined as input file name, if no explicit value
                        continue
                    raise Exception(f"Could not resolve '{ref.scope or ref.head()}' for {self.file}")
            reqs[ref] = scope
        self.requirements = reqs

    def find_pending_requirement(self) -> Optional['FileContext']:
        for ref, scope in self.requirements.items():
            for ctx in scope.get_value(ref, scope=scope):
                if not ctx.state.is_finished():
                    return ctx
        return None

    def create_missing_outputs(self, required_outputs: List[ValueName]) -> Iterable['FileContext']:
        new_outputs = []
        if self.global_context() and INPUT_VALUE not in self.outputs:
            # this is global scope, implicitly create IN outputs, if there are rules IN => xxx ...
            for requ in required_outputs:
                if self.manager.rules.get_rules_to_find(INPUT_VALUE, requ):
                    out_file = self.add_output(name=INPUT_VALUE)
                    new_outputs.append(out_file)
                    break
        start = self.__end()
        for requ in required_outputs:
            added = set()
            for rule in self.manager.rules.get_rules_to_find(start, requ):
                # check if we need new output for this rule
                check_unique = not rule.input_file or rule.end not in added
                if check_unique and rule.end in self.outputs:
                    continue
                t_file = self.__get_rule_output_file(rule)
                out_file = self.add_output(name=rule.end, file=t_file, append=rule.input_file is not None)
                if rule.input_file:
                    # rule is not a choice, but known
                    out_file.context_rule = rule
                new_outputs.append(out_file)
                added.add(rule.end)
        return new_outputs

    def __get_rule_output_file(self, rule: Rule) -> DataFile:
        if self.global_context():
            # root file
            if rule.input_file:
                b_file = self.manager.get_target(rule.input_file, rule.end.name)
            elif rule.is_set(RuleOption.OUTPUT_DIRECTORY):
                b_file = self.file / rule.get_option(RuleOption.OUTPUT_DIRECTORY) / rule.end.name
            else:
                b_file = self.file / rule.end.name
        else:
            # the normal location
            b_file = self.manager.get_target(self.file, rule.end.name)
        return b_file

    def __outputs_to_go(self, required_outputs: Optional[List[ValueName]]) -> List[ValueName]:
        go_outputs = []
        if required_outputs is not None:
            start = self.__end()
            covered = set()

            # check if value just available here (use collected value)
            for requ in required_outputs:
                requ_links = self.outputs.links.get(requ)
                if requ_links and all(rl.state.is_finished() for rl in requ_links.iterate_contexts()):
                    go_outputs.append(requ)
                    covered.add(requ)

            if self.global_context():
                # check if value reached through IN files
                for requ in required_outputs:
                    if requ in covered:
                        continue
                    if requ == INPUT_VALUE:
                        go_outputs.append(INPUT_VALUE)
                        break
                    if self.manager.rules.get_rules_to_find(INPUT_VALUE, requ):
                        go_outputs.append(INPUT_VALUE)
                        break

            # finally check if rules required to resolve the value
            for requ in required_outputs:
                for rule in self.manager.rules.get_rules_to_find(start, requ):
                    if rule.end not in covered:
                        go_outputs.append(rule.end)
                        covered.add(rule.end)
        else:
            # only existing outputs
            go_outputs.extend(self.outputs.links.keys())
        return go_outputs

    def get_value(self, reference: RuleReference, scope: Optional['FileContext'] = None) -> List['FileContext']:
        if not scope or scope == self:
            value = self.value_cache.get(reference.name)
            if value is not None:
                return value
        head = reference.head()
        value = []
        exclude = scope.parent if scope and scope.name == EXPAND_FILES else scope
        if self.state.is_finished():
            if self.state == FileState.DROPPED:
                return value
            # create all outputs leading to the named value
            to_go = self.__outputs_to_go([head])
            if EXPAND_FILES in self.outputs:
                to_go += [EXPAND_FILES]
            for n in to_go:
                ex_output = self.outputs[n]
                if ex_output:
                    # existing output
                    for c in ex_output.iterate_contexts():
                        if c.state == FileState.DROPPED:
                            continue
                        if exclude and c.name == exclude.name:
                            continue
                        if c.name == reference.name:
                            # value found
                            value.append(c)
                        elif c.name == head:
                            # head match, but tail to go
                            c_value = c.get_value(reference.tail(), scope)
                            value.extend(c_value)
                        else:
                            # keep looking for the value
                            c_value = c.get_value(reference, scope)
                            value.extend(c_value)
                else:
                    # create new output
                    out_file = self.add_output(name=n)
                    value.append(out_file)
            if not scope or scope == self:
                if all([v.state.is_finished() for v in value]):
                    # can cache value
                    self.value_cache[reference.name] = value
        else:
            # context not ready, resolution must wait
            value.append(self)
        return value

    def global_context(self) -> bool:
        return self.parent is None and not self.context_rule

    def __end(self) -> ValueName:
        if self.context_rule:
            return self.context_rule.end
        if self.parent and not self.parent.global_context():
            return self.parent.__end()  # expanded file
        if self.global_context():
            return GLOBAL
        return INPUT_VALUE

    def get_immediate_to_resolve(self, value: ValueName) -> List[ValueName]:
        # deduplicate names
        names = {}
        rules = self.manager.rules.get_rules_to_find(self.__end(), value)
        for r in rules:
            names[r.end] = r
        if self.global_context() and self.manager.rules.get_rules_to_find(INPUT_VALUE, value):
            # global context has access to all input files
            names[INPUT_VALUE] = None
        return list(names.keys())

    def get_rules_for(self, name: ValueName) -> Iterable[Rule]:
        return self.manager.rules.get_rules(self.__end(), end=name)

    def resolve_input_files(self, rule: Rule,
                            metadata: Optional[FileMetadata]) -> Tuple[bool, Dict[str, List[DataFile]]]:
        context = self
        in_files = {}
        batch_root = self.batch_root
        changes = metadata is None
        if not batch_root and rule.start != GLOBAL:
            files = [context.parent.file]
            in_files[rule.start.name] = files
            changes = changes or context.parent.created
            if rule.start == INPUT_VALUE and not changes:
                # input changed?
                changes = metadata.check_for_new_data_files(files)

        if batch_root:
            # batch rule predicate values resolved in given scopes
            end_ref = RuleReference(rule.end)
            end_scope = context.requirements.get(end_ref)
            end_values = end_scope.get_value(end_ref)
            predicate_scopes = [c.parent for c in end_values]
        else:
            predicate_scopes = None

        for ref, scope in context.requirements.items():
            if batch_root and ref.name == rule.end:
                # the start rule value resolved by tested locations
                key = rule.start
                values = predicate_scopes
            elif batch_root and not ref.scope:
                # values relative to tested locations
                key = ref.name
                values = []
                for b_scope in predicate_scopes:
                    values.extend(b_scope.get_value(ref))
            else:
                key = ref.name
                values = scope.get_value(ref, scope=scope)

            in_files[key.name] = [f.file for f in values]
            changes = changes or any(v.created for v in values)
            if ref.name == INPUT_VALUE and not changes:
                # input changed?
                changes = metadata.check_for_new_data_files(in_files[key.name])

        if INPUT_VALUE.name not in in_files:
            # IN stores topmost 'value' file (used to resolve file name for predicates)
            input_file = context.resolve_in_file()
            in_files[INPUT_VALUE.name] = [input_file]

        if rule.predicate:
            rule.predicate.local_values(context.file.parent(), in_files)

        return changes, in_files

    def resolve_parent(self, value: ValueName) -> Optional['FileContext']:
        if value == self.name:
            return self
        if self.parent:
            return self.parent.resolve_parent(value)
        return None

    def resolve_scope(self, value: ValueName) -> Optional['FileContext']:
        if value == self.name or value in self.possible_values:
            return self
        if self.name == EXPAND_FILES and value == self.parent.name:
            # return the expanded value, not the directory, when resolving scope
            return self
        if self.parent:
            return self.parent.resolve_scope(value)
        return None

    def resolve_in_file(self, in_dir: Optional[bool] = None) -> DataFile:
        if not self.parent or self.parent.global_context():
            # IN or root rule
            if self.context_rule and self.context_rule.input_file:
                return self.context_rule.input_file
            return self.file
        if self.name == EXPAND_FILES and not in_dir:
            return self.file  # file output
        if self.file.is_dir() and in_dir:
            return self.file  # directory output
        return self.parent.resolve_in_file(
            self.context_rule.is_set(RuleOption.DIRECTORY) if self.context_rule and in_dir is None else in_dir)

    def expand_to_files(self) -> 'Links':
        links = self.outputs[EXPAND_FILES]
        if links is None:
            links = Links()
            for d_file in self.directory_of:
                d_out = FileContext(self.manager, d_file, EXPAND_FILES, self, state=FileState.READY)
                # d_out.context_rule = self.context_rule # not copied
                d_out.possible_values = self.possible_values
                d_out.created = self.created
                links.append(d_out)
            self.outputs[EXPAND_FILES] = links
        return links

    def post_process(self, required_outputs: List[ValueName]) -> Iterable['FileContext']:
        if self.context_rule:
            self.possible_values = self.manager.rules.list_reachable_values(self.context_rule.end)
        # create required outputs, if any
        if not self.possible_values:
            return []
        self.create_missing_outputs(required_outputs)
        return self.outputs.iterate_contexts()

    def __truediv__(self, name) -> 'FileContext':
        links = self.outputs[ValueName(name)]
        if not links or links.length() != 1:
            raise ValueError(f"Unknown context '{name}'")
        return links.head()

    def __repr__(self):
        try:
            path = self.file.as_string()
        except ValueError:
            path = f"PATH {self.file.path.as_posix()}"
        return f"{self.state} {path}"


class Links:
    def __init__(self, *items: FileContext):
        self.items: List[FileContext] = list(items) or []

    def iterate_contexts(self, exclude: Optional[FileContext] = None) -> Iterable[FileContext]:
        return self.items

    def length(self) -> int:
        return len(self.items)

    def append(self, context: FileContext) -> 'Links':
        self.items.append(context)
        return self

    def head(self) -> Optional[FileContext]:
        if self.items:
            return self.items[0]
        return None

    def __repr__(self):
        lines = [f.__repr__() for f in self.iterate_contexts()]
        return "\n".join(lines)


class ContextLinks:
    def __init__(self, name: ValueName = None, value: Links = None):
        self.links: Dict[ValueName, Links] = {}
        if name:
            self[name] = value

    def __setitem__(self, name: ValueName, value: Links):
        self.links[name] = value

    def __getitem__(self, name: ValueName) -> Optional[Links]:
        return self.links.get(name)

    def __contains__(self, item) -> bool:
        return item in self.links

    def iterate_contexts(self) -> Iterable[FileContext]:
        return itertools.chain(*[links.iterate_contexts() for links in self.links.values()])

    def __repr__(self):
        lines = []
        for rule, links in self.links.items():
            r = rule.name
            for f in links.iterate_contexts():
                lines.append(f"{r} = {f.state} {f.file}")
        return "\n".join(lines)
