import pathlib
from typing import List, Set, Iterable, Dict, Optional

from minion import file_lexer
from minion.predicate import RuleReference
from minion.interfaces import RuleLibrary, Rule, DataFile
from minion.rules import default_search_path, locate
from minion.value import ValueName, GLOBAL


class FileBasedRuleLibrary(RuleLibrary):
    def __init__(self, rule_file: DataFile, file_root: Optional[DataFile] = None):
        super().__init__()
        self.file_root = file_root
        self.rule_file = rule_file
        self.regressive_names: Set[ValueName] = set()
        if rule_file.is_dir():
            rules = self.__parse_rule_directory(rule_file.path)
            for r in rules:
                self.add_rule(r)
        if rule_file.is_file():
            rules = self.__parse_rule_file(rule_file.path)
            for r in rules:
                self.add_rule(r)

    @classmethod
    def locate(cls, rule_file: str, rule_search_paths: List[pathlib.Path] = None) -> 'FileBasedRuleLibrary':
        sp = default_search_path() if rule_search_paths is None else rule_search_paths
        path = locate(rule_file, sp)
        library = FileBasedRuleLibrary(DataFile.new(path))
        return library

    @classmethod
    def list_rule_files(cls, rule_search_paths: List[pathlib.Path] = None) -> List[pathlib.Path]:
        sp = default_search_path() if rule_search_paths is None else rule_search_paths
        res = []
        for path in sp:
            if path.name == '':
                continue  # skip .
            if path.is_dir():
                res.extend([f.relative_to(path) for f in path.glob("**/*.rules") if f.is_file()])
        return res

    def __used_names(self) -> Set[ValueName]:
        names = set()
        for r in self.list_rules():
            names.add(r.start)
            names.add(r.end)
        return names

    def add_rule(self, rule: Rule) -> 'RuleLibrary':
        self.dependency_cache = None
        names = self.__extract_names(rule)
        must_rename = sorted(names.intersection(self.regressive_names))
        if must_rename:
            # regressive names would cause conflict
            used_names = self.__used_names()
            for n in must_rename:
                new_n = self.__avoid_name_conflict(n, used_names)
                self.regressive_names.remove(n)
                self.regressive_names.add(new_n)
        return super().add_rule(rule)

    def import_rules(self, import_file: str, import_names: Dict[ValueName, ValueName]) -> Iterable[Rule]:
        self.dependency_cache = None
        location = self.rule_file.path if self.rule_file.is_dir() else self.rule_file.path.parent
        lib = self.locate(import_file, [location] + default_search_path())

        non_existing_names = set(import_names.keys()).difference(lib.__used_names())
        if non_existing_names:
            names = ", ".join([v.name for v in sorted(non_existing_names)])
            raise Exception(f"Imported rule set {import_file} does not contain names: {names}")

        # perform renames
        for old, new in import_names.items():
            if old != new:
                lib.__rename(old, new)
        imported_set = set(import_names.values())

        import_file_used_names = lib.__used_names()
        used_names = self.__used_names().union(imported_set)

        # rename values which not imported, but conflict
        imported_names = set()
        for n in sorted(import_file_used_names):
            if n in imported_set or n == GLOBAL:
                continue
            name = lib.__avoid_name_conflict(n, used_names)
            imported_names.add(name)

        # now can import all
        for r in lib.list_rules():
            self.add_rule(r)

        # if we add name with conflicting name, we must redefine these 'regressive' names
        self.regressive_names.update(imported_names)
        return lib.list_rules()

    def __avoid_name_conflict(self, name: ValueName, used_names: Set[ValueName]) -> ValueName:
        n_count = 1
        new_n = name
        while new_n in used_names:
            n_count += 1
            new_n = ValueName(f"{name.name}_{n_count}")
        if new_n != name:
            self.__rename(name, new_n)
        return new_n

    def __rename(self, old_name: ValueName, new_name: ValueName):
        rule_map = list(self.rules.items())
        for name, rules in rule_map:
            for r in rules:
                # change start and end, if required
                if r.start == old_name:
                    r.start = new_name
                if r.end == old_name:
                    r.end = new_name
                # update variable mappings
                if old_name.name in r.variables:
                    old_ref = r.variables[old_name.name]
                    r.variables[old_name.name] = RuleReference(name=new_name, scope=old_ref.scope)
                # update variable scopes
                new_vars = {}
                for n, ref in r.variables.items():
                    if ref.scope == old_name:
                        new_vars[n] = RuleReference(name=ref.name, scope=new_name)
                    else:
                        new_vars[n] = ref
                r.variables = new_vars
            # update mapping
            if name == old_name:
                del self.rules[name]
                self.rules[new_name] = rules

    def __parse_rule_directory(self, dir: pathlib.Path,) -> List[Rule]:
        rules = []
        if not dir.is_dir():
            return []
        for file in sorted(dir.iterdir()):
            if not file.is_file() or file.suffix != '.rules':
                continue
            file_rs = self.__parse_rule_file(file)
            rules.extend(file_rs)
        return rules

    def __parse_rule_file(self, file: pathlib.Path) -> List[Rule]:
        rules = []
        self.logger.debug(f"Read rule file {file.as_posix()}")
        with file.open("r") as f:
            file_lexer.parse(f.read(), file, library=self, file_root=self.file_root)
        return rules

    @classmethod
    def __extract_names(cls, from_rule: Rule) -> Set[ValueName]:
        names = set()
        names.add(from_rule.start)
        names.add(from_rule.end)
        for ref in from_rule.variables.values():
            names.add(ref.name)
            if ref.scope:
                names.add(ref.scope)
        return names
