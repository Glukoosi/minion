import pathlib
import sys
import threading
import time
from collections import deque
from typing import Optional, List, Dict, Set, Deque

from minion.builder import Builder, PreparedContext, ContextListener
from minion.context import FileContext, FileState
from minion.file import DataFile
from minion.file_manager import SubDirectoryFileManager
from minion.file_registry import DefaultFileRegistry
from minion.interfaces import Rule
from minion.predicate import PredicateValue
from minion.rule_library import FileBasedRuleLibrary
from minion.value import ValueName, GLOBAL, INPUT_VALUE


class Scheduler(ContextListener):
    def __init__(self, builder: Builder):
        self.builder = builder
        self.manager = builder.manager
        self.logger = self.manager.logger
        self.context_listeners: List[ContextListener] = [self]

        # context added here when it is evaluated
        self.contexts: Dict[DataFile, FileContext] = {
            builder.global_context.file: builder.global_context
        }
        for ctx in self.builder.contexts:
            self.contexts[ctx.file] = ctx
        self.contexts_readied = 0
        self.todo_queue: Deque[FileContext] = deque()
        # failed contexts
        self.failed: List[FileContext] = []

        self.max_threads = 1
        self.delay_after_apply = 0  # seconds
        self.threads: Dict[threading.Thread, PreparedContext] = {}
        self.lock = threading.Condition()

    def list_contexts(self) -> List[FileContext]:
        with self.lock:
            return list(self.contexts.values())

    def list_running(self) -> List[PreparedContext]:
        with self.lock:
            return list(self.threads.values())

    def get_existing_context(self, file: DataFile) -> Optional[FileContext]:
        with self.lock:
            context = self.contexts.get(file)
            while context and not context.state.is_finished():
                self.lock.wait(1.0)
        return context

    def apply_by_file(self, context: FileContext, by_file: DataFile):
        with self.lock:
            value = ValueName(by_file.name())
            self.builder.prepare_outputs(context, {value})
            self.__queue_ready_output(context)
            self.__do_schedule()

    def apply_all(self, context: FileContext):
        with self.lock:
            if context.global_context():
                roots = self.manager.rules.list_reachable_values(GLOBAL)
                self.apply_by_values(context, roots)
            self.builder.prepare_outputs(context, context.possible_values)
            self.__queue_ready_output(context)
            self.__do_schedule()

    def apply_by_values(self, context: FileContext, values: Set[ValueName]):
        with self.lock:
            self.builder.prepare_outputs(context, values)
            self.__queue_ready_output(context)
            self.__do_schedule()

    def get_from_queue(self) -> Optional[FileContext]:
        with self.lock:
            while True:
                self.__do_schedule()
                if self.todo_queue:
                    return self.todo_queue.popleft()
                if not self.threads:
                    return None
                self.lock.wait(1.0)

    def __do_schedule(self) -> bool:
        started = False
        while self.max_threads <= 0 or len(self.threads) < self.max_threads:
            pre = self.builder.get_to_evaluate()
            if not pre:
                return started > 0
            if self.max_threads <= 0:
                # no multithreading
                pre.run([self])
                return True
            context = pre.context
            # can loop here many times if multiple rules
            self.contexts[context.file] = context
            thread = threading.Thread(name=pre.context.file.as_string(), target=pre.run, args=[self.context_listeners])
            self.threads[thread] = pre
            self.logger.debug("start %s (%d/%d)", thread.name, len(self.threads), self.max_threads)
            thread.start()
            started += 1
        return started > 0

    def __queue_ready_output(self, context: FileContext):
        for out in context.outputs.iterate_contexts():
            if out.state.is_finished():
                self.todo_queue.append(out)

    def end_context(self, prepared: 'PreparedContext'):
        if self.max_threads <= 1:
            # in single thread, force order of events to stdout/stderr
            sys.stdout.flush()
        time.sleep(self.delay_after_apply)
        thread = threading.current_thread()
        context = prepared.context
        with self.lock:
            if thread in self.threads:
                del self.threads[thread]
            ready = self.builder.post_process(prepared)
            for r in ready:
                self.contexts[r.file] = r
                if r.state == FileState.READY:
                    self.contexts_readied += 1
                self.todo_queue.append(r)
            if context.error:
                self.failed.append(context)
            self.lock.notify_all()


class FacadeListener:
    def start_facade(self):
        pass

    def stop_facade(self):
        pass


class Facade:
    def __init__(self, builder: Builder):
        self.scheduler = Scheduler(builder)
        self.builder = builder
        self.manager = builder.manager
        self.logger = self.manager.logger
        self.listeners: List[FacadeListener] = []

    @classmethod
    def new(cls, root: DataFile, rules: List[Rule] = None, rule_file: Optional[DataFile] = None,
            sources: List[DataFile] = None) -> 'Facade':
        builder = Builder.new(root, rules=rules, rule_file=rule_file, sources=sources)
        return Facade(builder)

    @classmethod
    def in_directory(cls, project_name: str, directory: pathlib.Path, rule_file: str, sources: List[str]) -> 'Facade':
        rules = FileBasedRuleLibrary.locate(rule_file)
        registry = DefaultFileRegistry(directory, project_name=project_name, rules=rules)
        manager = SubDirectoryFileManager(registry)
        sources_f = []
        for f in sources:
            f_f = registry.get_file_by_path(f)
            if not f_f:
                raise Exception(f"File not in working directory: {f}")
            sources_f.append(f_f)
        builder = Builder(manager, sources_f)
        return Facade(builder)

    def start(self):
        if not self.builder.contexts and self.manager.rules.get_rules(INPUT_VALUE):
            raise Exception(f"No input files provided (value '{INPUT_VALUE.name}' used in rules)")
        for fl in self.listeners:
            fl.start_facade()

    def define(self, name: str, value: str):
        self.manager.registry.defined_values[name] = PredicateValue(name, string=value)

    def get_data_file(self, file: DataFile) -> Optional[DataFile]:
        context = self.get_context(file)
        return context.file if context else None

    def get_context(self, file: DataFile) -> Optional[FileContext]:
        context = self.scheduler.get_existing_context(file)
        if context:
            return context
        p_file = file.parent()
        if not p_file:
            return None
        parent_file = self.manager.get_data_file(p_file.path)
        parent = self.get_context(parent_file)
        if not parent:
            return None
        self.scheduler.apply_by_file(parent, by_file=file)
        while True:
            # drain ready list, but right context may also be created earlier...
            context = self.scheduler.get_from_queue()  # or self.scheduler.get_existing_context(file)
            if context is None:
                self.builder.check_for_deadlock()
                return  # no more candidate values
            if context.state == FileState.DROPPED:
                continue
            assert context.state == FileState.READY
            if context.file == file:
                return context

    def find_values(self, *values: ValueName) -> List[DataFile]:
        if not values:
            values = [r.end for r in self.manager.rules.get_targets()]
        files = [c.file for c in self.find_contexts(*values)]
        return sorted(files)

    def find_contexts(self, *values: ValueName) -> List[FileContext]:
        files = {}
        values_set = set(values)
        self.scheduler.apply_by_values(self.builder.global_context, values_set)
        self.__find_values(values_set, files)
        return list(files.values())

    def __find_values(self, values: Set[ValueName], files: Dict[DataFile, FileContext]):
        while True:
            context = self.scheduler.get_from_queue()
            if context is None:
                self.builder.check_for_deadlock()
                return  # no more candidate values
            if context.state == FileState.DROPPED:
                continue
            assert context.state == FileState.READY
            if context.name in values and context.file not in files:
                files[context.file] = context  # a value found!
            self.scheduler.apply_by_values(context, values)

    def build_all(self):
        self.scheduler.apply_all(self.builder.global_context)
        while True:
            context = self.scheduler.get_from_queue()
            if context is None:
                self.builder.check_for_deadlock()
                return  # all is ready
            if context.state == FileState.DROPPED:
                continue
            self.scheduler.apply_all(context)

    def stop(self):
        for fl in self.listeners:
            fl.stop_facade()
        if self.scheduler.failed:
            s = "\n".join([f"{c.file}\n  {str(c.error)}" for c in self.scheduler.failed])
            raise Exception(f"Failed to create some files:\n{s}")

        if not self.scheduler.contexts_readied:
            raise Exception(f"No input files matched any rules")

    def __truediv__(self, value_name: str) -> Optional[FileContext]:
        return self.builder / value_name
