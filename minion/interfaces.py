import itertools
import json
import logging
import pathlib
from datetime import datetime
from enum import Enum
from io import TextIOBase
from typing import Iterable, Optional, List, Any, Dict, Set, Tuple

from minion import predicate
from minion.predicate import RuleReference, PredicateValues, PredicateValue
from minion.file import DataFile
from minion.value import ValueName, GLOBAL, INPUT_VALUE


class RuleOption(Enum):
    """Rule options"""
    BATCH = "batch"
    TARGET = "target"
    DIRECTORY = "dir"
    SCOPE = "scope"
    OUTPUT_DIRECTORY = "out-dir"

    def __str__(self):
        return self.value.__str__()


class Rule:
    """Rule interface"""
    def __init__(self, start: ValueName, end: ValueName):
        self.start = start
        self.end = end
        self.input_file: Optional[DataFile] = None
        self.predicate: Optional[predicate.PredicateOperation] = None
        self.local_variables: Set[ValueName] = set()
        self.variables: Dict[str, RuleReference] = {}
        self.options: Dict[RuleOption, Any] = {}
        self.digest: Optional[str] = None
        self.source: Optional[Tuple[str, int]] = None

    def finish(self) -> 'Rule':
        """Finish the rule after its attributes are ready"""
        return self

    def is_set(self, option: RuleOption) -> bool:
        """Is the given option set?"""
        return self.options.get(option, False)

    def get_option(self, option: RuleOption) -> Optional[Any]:
        """Get the value of the given option or None"""
        return self.options.get(option, None)

    def is_dependent(self) -> bool:
        return len(self.variables) > 1 or self.start.name not in self.variables

    def pack_files(self) -> bool:
        return self.options.get(RuleOption.BATCH, False) or self.options.get(RuleOption.SCOPE, False)

    def alias(self) -> bool:
        """Is the rule just alias for a file without processing"""
        return False

    def get_command_variables(self) -> Set[str]:
        return set(self.variables.keys())

    def apply(self, predicate: PredicateValues, out_file: DataFile, meta: 'FileMetadata', timeout: float,
              logger: logging.Logger):
        """Apply the rule"""
        raise NotImplementedError()

    def to_string(self) -> str:
        """Full string representation of the rule"""
        raise NotImplementedError()

    def __str__(self):
        if self.input_file:
            key_w = 'file'
            start = self.input_file.as_string()
        else:
            key_w = 'rule'
            start = self.start.name
        if self.is_set(RuleOption.SCOPE):
            return f"{self.end} <= {start} # {self.digest}"
        return f"{key_w} {start} => {self.end} # {self.digest}"

    def __eq__(self, other) -> bool:
        return isinstance(other, Rule) and self.digest == other.digest and self.start == other.start and \
            self.end == other.end

    def __hash__(self):
        return self.digest.__hash__() + self.start.__hash__() + self.end.__hash__()

    def __lt__(self, other):
        if self.start != other.start:
            return self.start.__lt__(other.start)
        return self.end.__lt__(other.end)


JSON_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'


class FileLog:
    """Log entry for a file"""
    def __init__(self, path: str, digest: str, timestamp: Optional[datetime] = None):
        self.path = path
        self.digest = digest
        self.timestamp = timestamp

    @classmethod
    def for_file(cls, file: DataFile) -> 'FileLog':
        """Create log entry for a file"""
        digest = file.sha256_digest() if file.is_file() else ''
        timestamp = datetime.fromtimestamp(file.path.stat().st_mtime)
        return FileLog(file.as_string(), digest, timestamp)

    @classmethod
    def format_time(cls, value: datetime) -> str:
        s = value.strftime(JSON_TIME_FORMAT)
        if value.year < 1000:
            # make sure that years < 1000 are correctly formatted
            # (https://bugs.python.org/issue13305)
            year_mark = s.index('-')
            s = f"{value.year:04d}{s[year_mark:]}"
        return s

    def to_json(self) -> Dict[str, Any]:
        """Write as JSON"""
        js = {
            'path': self.path
        }
        if self.digest:
            js['digest'] = self.digest
        if self.timestamp:
            timestamp_s = self.format_time(self.timestamp)
            js['timestamp'] = timestamp_s
        return js

    def __repr__(self) -> str:
        return json.dumps(self.to_json(), indent=4)


class FileMetadata:
    """File metadata, read and written to metadata file"""
    def __init__(self, file: DataFile, input: List[FileLog] = None, rule: Optional[Rule] = None, full_data=False):
        self.full_data = full_data
        self.file = file
        self.input: List[FileLog] = input or []
        self.output: List[FileLog] = []
        self.rule = rule
        self.rule_digest = rule.digest if rule else ''
        self.exit_code = 0
        self.log: List[(str, str)] = []
        self.timestamp = datetime.now()

    def check_for_new_data_files(self, files: Iterable[DataFile]) -> bool:
        if not files:
            return False
        meta_mod = self.timestamp.timestamp()
        for f in files:
            f_mod = f.path.stat().st_mtime
            diff = meta_mod - f_mod
            if diff < -0.01:
                return True  # data file is newer than meta file, by a margin
        return False

    def to_json(self) -> Dict[str, Any]:
        """Write to JSON"""
        js = {}
        if self.input:
            js['input'] = [i.to_json() for i in self.input]
        if self.rule:
            js['start'] = self.rule.start.name
            js['end'] = self.rule.end.name
            js['rule-digest'] = self.rule.digest
        timestamp_s = FileLog.format_time(self.timestamp)
        js['timestamp'] = timestamp_s
        if self.exit_code:
            js['exit_code'] = self.exit_code
        if self.log:
            js['log'] = self.log
        if self.output:
            js['output'] = [o.to_json() for o in self.output]
        return js

    def __repr__(self):
        return json.dumps(self.to_json())


class RuleLibrary:
    def __init__(self, rules: List[Rule] = None):
        self.rules: Dict[ValueName, List[Rule]] = {}
        self.rules_by_digest: Dict[str, Rule] = {}
        self.rule_timeout = 60.0  # 60 seconds
        self.logger = logging.getLogger('rules')
        for r in rules or []:
            self.add_rule(r)
        self.dependency_cache: Optional[Dict[Tuple[ValueName, ValueName], List[Rule]]] = None
        self.reachability_cache: Dict[ValueName, Set[ValueName]] = {}

    def list_rules(self) -> Iterable[Rule]:
        rules = {}
        for rs in self.rules.values():
            for r in rs:
                rules[r] = r
        return rules.keys()

    def get_targets(self) -> List[Rule]:
        r = []
        first = None
        for rs in self.rules.values():
            if not first and rs:
                first = rs[0]
            r.extend([r for r in rs if r.is_set(RuleOption.TARGET)])
        if not r and first:
            # just pick the first target
            r = [first]
        return r

    def get_rule(self, digest: str) -> Optional[Rule]:
        return self.rules_by_digest.get(digest)

    def get_rules(self, start: ValueName, end: Optional[ValueName] = None) -> Iterable[Rule]:
        rules = self.rules.get(start)
        if not rules:
            return []
        if end:
            rules = list(filter(lambda r: r.end == end, rules))
        return rules

    def list_reachable_rules(self, start: ValueName) -> Iterable[Rule]:
        values = self.list_reachable_values(start)
        rules = set()
        for rs in self.rules.values():
            for r in rs:
                if r.end in values:
                    rules.add(r)
        return rules

    def list_reachable_values(self, start: ValueName) -> Set[ValueName]:
        self.__ensure_dependency_cache()
        values = self.reachability_cache.get(start) or set()
        return values

    def get_rules_to_find(self, start: ValueName, to_value: ValueName) -> Iterable[Rule]:
        self.__ensure_dependency_cache()
        return self.dependency_cache.get((start, to_value)) or []

    def is_reachable(self, value: ValueName) -> bool:
        self.__ensure_dependency_cache()
        return (INPUT_VALUE, value) in self.dependency_cache or (GLOBAL, value) in self.dependency_cache

    def __list_all_rules_to_go(self, from_rules: Iterable[Rule], to_go: Set[Rule]) -> Set[Rule]:
        for r in from_rules or []:
            if r in to_go:
                continue
            to_go.add(r)
            sub_rules = self.get_rules(r.end)
            self.__list_all_rules_to_go(sub_rules or [], to_go)
        return to_go

    def add_rule(self, rule: Rule) -> 'RuleLibrary':
        self.dependency_cache = None
        self.rules.setdefault(rule.start, []).append(rule)
        self.rules_by_digest[rule.digest] = rule
        self.logger.debug(rule)
        return self

    def import_rules(self, import_file: str, import_names: Dict[ValueName, ValueName]) -> Iterable[Rule]:
        raise NotImplementedError()

    def __ensure_dependency_cache(self):
        if self.dependency_cache is not None:
            return
        self.dependency_cache = {}
        self.reachability_cache = {}
        for name, rules in self.rules.items():
            for r in rules:
                ends = self.__list_values_to_reach(r.end, {r.end})
                self.reachability_cache.setdefault(r.start, set()).update(ends)
                for e in ends:
                    r_e_list = self.dependency_cache.setdefault((r.start, e), [])
                    if r not in r_e_list:
                        r_e_list.append(r)

    def __list_values_to_reach(self, from_value: ValueName, values: Set[ValueName]) -> Set[ValueName]:
        for r in self.get_rules(from_value) or []:
            if r.end in values:
                continue
            values.add(r.end)
            self.__list_values_to_reach(r.end, values)
        return values

    def get_value_nodes(self) -> Dict[ValueName, List[Rule]]:
        nodes = {}
        for rule in self.list_rules():
            nodes.setdefault(rule.start, []).append(rule)
            nodes.setdefault(rule.end, [])
        return nodes

    def __repr__(self):
        return "\n".join([r.__str__() for r in itertools.chain(*self.rules.values())])


class FileRegistry:
    """Can assign metadata to files, knows sources for derived files and knows which rules apply where"""
    def __init__(self, rules: RuleLibrary, root_file: DataFile):
        self.root_file = root_file
        self.rules = rules
        self.defined_values: Dict[str, PredicateValue] = {}
        self.logger = logging.getLogger('registry')

    def get_file_by_path(self, path: str) -> Optional[DataFile]:
        return None

    def reverse_path(self, path: pathlib.Path) -> List[pathlib.Path]:
        path_list = ([path] + list(path.parents))
        path_list.reverse()
        for i, p in enumerate(path_list):
            if p == self.root_file.root:
                return path_list[i:]
        raise Exception(f"The given path not inside minion directory: {path.as_posix()}")

    def apply_rule(self, predicate: PredicateValues, out_file: DataFile, rule: Rule,
                   logger: logging.Logger) -> FileMetadata:
        raise NotImplementedError

    def relocate_rule(self, in_file: DataFile, interim_file: DataFile, out_file: DataFile, rule: Rule) -> DataFile:
        raise NotImplementedError

    def write_metafile(self, meta_file: FileMetadata):
        raise NotImplementedError

    def clean(self, file: DataFile):
        pass

    def get_metadata(self, file: DataFile, parent_meta: bool = False, full_data=True) -> Optional[FileMetadata]:
        return None

    def _read_file_metadata(self, file: DataFile, json: Dict[str, Any], full_data: bool) -> FileMetadata:
        m = FileMetadata(file, full_data=full_data)
        if full_data:
            for js in json.get('input', []):
                src = js['path']
                digest = js.get('digest', '')
                timestamp = datetime.strptime(js['timestamp'], JSON_TIME_FORMAT) if 'timestamp' in js else None
                log = FileLog(src, digest, timestamp)
                m.input.append(log)
        m.rule_digest = json.get('rule-digest', '')
        m.rule = self.rules.get_rule(m.rule_digest)  # None, if digest does not match
        m.timestamp = datetime.strptime(json['timestamp'], JSON_TIME_FORMAT)
        m.exit_code = json.get('exit_code', 0)
        m.log = json.get('log', [])
        for js in json.get('output', []):
            dst = js['path']
            digest = js.get('digest', '')
            timestamp = None
            if full_data:
                timestamp = datetime.strptime(js['timestamp'], JSON_TIME_FORMAT) if 'timestamp' in js else None
            log = FileLog(dst, digest, timestamp)
            m.output.append(log)
        return m

    def get_batch_dir(self) -> pathlib.Path:
        return self.root_file.relative_path

    def explain(self, writer: TextIOBase, file: DataFile, with_prints: bool = False):
        raise NotImplementedError()


class FileManager:
    """Knows where to place derived files by rules"""
    def __init__(self, registry: FileRegistry):
        self.root_file = registry.root_file
        self.registry = registry
        self.rules = self.registry.rules
        self.logger = logging.getLogger('file-manager')
        self.ignore_changes = False

    def is_data_file(self, file: DataFile) -> bool:
        return False

    def get_sub_data_files(self, file: DataFile, recurse=False) -> List[DataFile]:
        return []

    def get_data_file(self, path: pathlib.Path) -> DataFile:
        raise NotImplemented()

    def is_applied_source_file(self, file: DataFile) -> bool:
        return False

    def get_target(self, file: DataFile, name: str = None) -> Optional[DataFile]:
        raise NotImplemented()

    def get_existing_targets(self, file: DataFile) -> Iterable[DataFile]:
        return []

    def apply_rule(self, predicate: PredicateValues, out_file: DataFile, rule: Rule,
                   logger: logging.Logger) -> FileMetadata:
        return self.registry.apply_rule(predicate, out_file, rule)

    def print_hierarchy(self, writer: TextIOBase, file: DataFile):
        raise NotImplemented()
