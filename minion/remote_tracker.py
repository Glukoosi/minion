import json
import logging
import pathlib
import random
import socketserver
import string
import threading
import time
from http import server
from typing import Optional, Dict, Tuple, List, Set, Iterable
import urllib.parse

from minion.builder import ContextListener, PreparedContext
from minion.context import FileState
from minion.file_facade import Scheduler, FacadeListener, Facade


class ThreadedHTTPServer(socketserver.ThreadingMixIn, server.HTTPServer):
    """Handle requests in a separate thread."""


def make_handler(debug_server: 'DebugServer'):
    """Create HTTP handler with access to debugger"""
    class Handler(server.BaseHTTPRequestHandler):
        def do_POST(self):
            if self.path.startswith(RemoteTracker.BREAKPOINTS_PREFIX):
                debug_server.handle_breakpoint(self, remove=False)
            else:
                self.send_error(404)

        def do_GET(self):
            if self.path.startswith('/rules'):
                debug_server.handle_list_rules(self)
            elif self.path.startswith(RemoteTracker.EVENTS_PREFIX):
                debug_server.handle_get_event(self)
            elif self.path.startswith(RemoteTracker.CONTEXTS_PREFIX):
                debug_server.handle_context(self)
            elif self.path.startswith('/pause'):
                debug_server.handle_pause_value(self, set_value=False)
            else:
                self.send_error(404)

        def do_PUT(self):
            if self.path.startswith(RemoteTracker.EVENTS_PREFIX):
                debug_server.handle_event_continue(self)
            elif self.path.startswith('/pause'):
                debug_server.handle_pause_value(self, set_value=True)
            else:
                self.send_error(404)

        def do_DELETE(self):
            if self.path.startswith(RemoteTracker.BREAKPOINTS_PREFIX):
                debug_server.handle_breakpoint(self, remove=True)
            else:
                self.send_error(404)

        def log_message(self, fmt, *args):
            pass  # do not log here

    return Handler


class PauseCondition:
    """Condition for pausing a context"""
    def __init__(self, *states: FileState):
        self.states: Set[FileState] = set(states)

    def pause(self, state: FileState):
        return state in self.states

    def __str__(self):
        return " ".join(s.name for s in self.states)


class Breakpoint:
    """A breakpoint in file and line"""
    def __init__(self, file: pathlib.Path, line: int, condition: PauseCondition):
        self.file = file
        self.line = line
        self.condition = condition

    def __str__(self):
        return f"{self.file.as_posix()}:{self.line} {self.condition}"


class Pending:
    """A pending event"""
    def __init__(self, number: int, point: Optional[Breakpoint] = None, context: Optional[PreparedContext] = None,
                 at_start=False, at_stop=False):
        self.number = number
        self.point = point
        self.context = context
        self.at_start = at_start
        self.at_stop = at_stop


class RemoteTracker(FacadeListener, ContextListener):
    BREAKPOINTS_PREFIX = '/breakpoints/'
    CONTEXTS_PREFIX = '/contexts/'
    EVENTS_PREFIX = '/events'  # also without /

    """Debugger with Web API"""
    def __init__(self, scheduler: Scheduler, local_addr: Optional[str], api_key: Optional[str]):
        super().__init__()
        self.scheduler = scheduler
        self.logger = logging.getLogger("api-server")
        self.lock = threading.Condition()
        self.auto_pause: Optional[PauseCondition] = PauseCondition(FileState.EVALUATE)
        if local_addr:
            addr_split = local_addr.partition(':')
            self.local_addr = addr_split[0] if addr_split[2] else '127.0.0.1'
            self.local_port = int(addr_split[2] if addr_split[2] else addr_split[0])
        else:
            self.local_addr = '127.0.0.1'
            self.local_port = 0  # automatically selected
        self.api_key = api_key
        if not api_key:
            self.api_key = ''.join(random.SystemRandom().choice(
                string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(32))

        self.breakpoints: Dict[Tuple[pathlib.Path, int], Breakpoint] = {}
        self.pending: Dict[int, Pending] = {}
        self.event_counter = 0
        self.httpd = ThreadedHTTPServer((self.local_addr, self.local_port), make_handler(self))
        self.local_port = self.httpd.server_port
        self.server_stopped = False
        self.logger.info("API at %s:%s key=%s", self.local_addr, self.local_port, self.api_key)

    @classmethod
    def attach(cls, facade: Facade, bind_address: Optional[str], api_key: Optional[str]) -> 'RemoteTracker':
        """Attach API to a facade and start the API server"""
        d_server = RemoteTracker(facade.scheduler, bind_address, api_key)
        facade.listeners.append(d_server)  # to capture start/stop
        facade.scheduler.context_listeners.append(d_server)  # to capture each rule

        def server_loop():
            d_server._serve_forever()
            with d_server.lock:
                d_server.server_stopped = True
                d_server.lock.notify_all()

        runner = threading.Thread(target=server_loop)
        runner.daemon = True
        runner.start()
        return d_server

    def wait_to_stop(self):
        """Wait for API server to stop"""
        with self.lock:
            while not self.server_stopped:
                self.lock.wait()

    def _serve_forever(self):
        self.httpd.serve_forever()

    def start_facade(self):
        if not self.auto_pause:
            return  # theoretical, as no time to set to false
        self.logger.info("Waiting for 'continue'")
        with self.lock:
            num = self.event_counter
            self.logger.info("Pause at start event=%d", num)
            self.event_counter += 1
            self.pending[num] = Pending(num, at_start=True)
            while self.pending.get(num):
                self.lock.wait()

    def start_context(self, context: 'PreparedContext'):
        rule = context.context.context_rule
        if not rule:
            return
        state = context.context.state
        with self.lock:
            break_here = False
            if self.auto_pause:
                break_here = self.auto_pause.pause(state)
            bp = None
            if not break_here:
                bp_key = pathlib.Path(rule.source[0]), rule.source[1]
                bp = self.breakpoints.get(bp_key)
                break_here = bp and bp.condition.pause(state)
            if break_here:
                num = self.event_counter
                self.logger.info("Pause at %s:%d event=%d", rule.source[0], rule.source[1], num)
                self.event_counter += 1
                self.pending[num] = Pending(num, bp, context)
                self.lock.notify_all()
                while self.pending.get(num):
                    self.lock.wait()
        return

    def handle_pause_value(self, handler: server.BaseHTTPRequestHandler, set_value: bool):
        """Handle pause flag put or get"""
        if not self._check_request(handler):
            return
        self._send_headers(handler)
        if set_value:
            path, qs = self._parse_path(handler)
            pause_flag = qs['value'] == 'true'
            self.logger.info("Set pause=%s", pause_flag)
            if pause_flag:
                self.auto_pause = PauseCondition(FileState.EVALUATE)
            else:
                self.auto_pause = None
        else:
            js = {
                'value': self.auto_pause is not None
            }
            handler.wfile.write(json.dumps(js, indent='    ').encode('utf8'))

    def handle_event_continue(self, handler: server.BaseHTTPRequestHandler):
        """Handle pending event continue"""
        if not self._check_request(handler, allow_set_pause=True):
            return
        full_path, qs = self._parse_path(handler)
        path = full_path[len(RemoteTracker.EVENTS_PREFIX):]
        action = qs.get('action')
        if action != "continue":
            handler.send_error(400)  # Bad request
            return
        with self.lock:
            if path.startswith('/'):
                num = int(path[1:])
                if num not in self.pending:
                    handler.send_error(404)  # not found
                    return
            else:
                num = -1
            self._send_headers(handler)
            if num != -1:
                # just continue one pending breakpoint
                self.logger.info("Continue event=%d", num)
                del self.pending[num]
            else:
                # continue all
                for n in self.pending.keys():
                    self.logger.info("Continue event=%d", n)
                self.pending.clear()
            self.lock.notify_all()

    def handle_breakpoint(self, handler: server.BaseHTTPRequestHandler, remove=False):
        """Handle post or delete a breakpoint"""
        if not self._check_request(handler):
            return
        path, qs = self._parse_path(handler)
        file = pathlib.Path(path[len(RemoteTracker.BREAKPOINTS_PREFIX):])
        line = int(qs.get('line', '0'))
        bp_key = (file, line)
        if remove:
            self.logger.info("Remove breakpoint %s:%d", file, line)
            if bp_key in self.breakpoints:
                del self.breakpoints[bp_key]
        else:
            cond = PauseCondition(FileState.EVALUATE)
            bp = Breakpoint(file, line, cond)
            self.logger.info("Place breakpoint %s:%d %s", file, line, bp)
            self.breakpoints[bp_key] = bp
        self._send_headers(handler)
        with self.lock:
            self.auto_pause = False
            self.lock.notify_all()

    def handle_get_event(self, handler: server.BaseHTTPRequestHandler):
        """Handle get pending event"""
        if not self._check_request(handler):
            return
        full_path, qs = self._parse_path(handler)
        timeout = int(qs.get('timeout', '0')) / 1000
        path = full_path[len(RemoteTracker.EVENTS_PREFIX):]
        with self.lock:
            list_all = False
            if path.startswith('/latest'):
                # pick latest event
                num = max(self.pending.keys()) if self.pending else self.event_counter
            elif path:
                # pick event by number
                num = int(path[1:])
            else:
                # list all events, wait for one if none available
                list_all = True
                num = max(self.pending.keys()) if self.pending else self.event_counter
            start_time = time.time()
            while num not in self.pending:
                waited_time = time.time() - start_time
                if waited_time < timeout:
                    # this event has not happened yet, wait...
                    self.lock.wait(timeout - waited_time)
                else:
                    break
            if num not in self.pending and not list_all:
                handler.send_error(404)  # not found
                return
            if list_all:
                p = list(self.pending.values())
            else:
                p = [self.pending[num]]
        self._send_headers(handler)
        if list_all:
            self.logger.info("Get all events")
            js_list = [self._get_event(ev) for ev in p]
            js = {'events': js_list}
        else:
            self.logger.info("Get event=%d", p[0].number)
            js = self._get_event(p[0])
        handler.wfile.write(json.dumps(js, indent='    ').encode('utf8'))

    def _get_event(self, pending: Pending) -> Dict:
        p = pending
        # create return JSON
        js = {
            'event': p.number
        }
        if p.at_start:
            js['start'] = True
        if p.at_stop:
            js['stop'] = True
        if p.context:
            ct = {
                'state': p.context.context.state.__str__(),
                'path': p.context.context.file.as_string(),
            }
            rule = p.context.context.context_rule
            if rule:
                ct['start'] = rule.start.name
                ct['end'] = rule.end.name
                ct['rule-digest'] = rule.digest
                if rule.source:
                    ct['source'] = {'file': rule.source[0], 'line': rule.source[1]}
                js['context'] = ct
        if p.point:
            br = {
                'breakpoint': {'file': p.point.file.as_posix(), 'line': p.point.line},
            }
            js['break'] = br
        return js


    def handle_context(self, handler: server.BaseHTTPRequestHandler):
        """Handle get context information"""
        if not self._check_request(handler):
            return
        path, _ = self._parse_path(handler)
        path_str = path[len(RemoteTracker.CONTEXTS_PREFIX):]
        file = self.scheduler.manager.get_data_file(pathlib.Path(path_str))
        ctx = self.scheduler.contexts.get(file)
        if not ctx:
            handler.send_error(404)  # not found
            return
        self._send_headers(handler)
        js = {
            'path': file.as_string(),
            'state': ctx.state.__str__()
        }
        rule = ctx.context_rule
        if rule:
            js['start'] = rule.start.name
            js['end'] = rule.end.name
            js['rule-digest'] = rule.digest
        reqs = []
        _, in_files = ctx.resolve_input_files(ctx.context_rule, None)
        for ref, i_files in in_files.items():
            reqs.append({
                'name': ref,
                'paths': [f.as_string() for f in i_files]
            })
        js['values'] = reqs
        handler.wfile.write(json.dumps(js, indent='    ').encode('utf8'))

    def handle_list_rules(self, handler: server.BaseHTTPRequestHandler):
        """Handle list rules"""
        if not self._check_request(handler):
            return
        self._send_headers(handler)
        resp = []
        for r in self.scheduler.builder.registry.rules.list_rules():
            rj = {
                'start': r.start.name,
                'end': r.end.name,
                'digest': r.digest,
            }
            if r.source:
                rj['source'] = {'file': r.source[0], 'line': r.source[1]}
            resp.append(rj)
        handler.wfile.write(json.dumps(resp, indent='    ').encode('utf8'))

    def stop_facade(self):
        if self.auto_pause:
            self.logger.info("Waiting for 'continue'")
            with self.lock:
                num = self.event_counter
                self.logger.info("Pause at start event=%d", num)
                self.event_counter += 1
                self.pending[num] = Pending(num, at_stop=True)
                while self.pending.get(num):
                    self.lock.wait()
        self.logger.info("API shutdown")
        self.httpd.shutdown()
        self.wait_to_stop()

    def _check_request(self, handler: server.BaseHTTPRequestHandler, allow_set_pause=False) -> bool:
        req_key = handler.headers.get('X-Api-Key')
        if req_key != self.api_key:
            handler.send_error(401)
            return False
        self.logger.debug("Debug %s", handler.requestline)
        if allow_set_pause:
            # convenient to set pause flag with active stuff
            _, qs = self._parse_path(handler)
            pause_value = qs.get('pause')
            if pause_value:
                self.auto_pause = pause_value == 'true'
                self.logger.info("Set pause=%s", self.auto_pause)
        return True

    def _parse_path(self, handler: server.BaseHTTPRequestHandler) -> Tuple[str, Dict[str, str]]:
        parsed = urllib.parse.urlparse(handler.path)
        qs = {k: "\n".join(v) for k, v in urllib.parse.parse_qs(parsed.query).items()}
        return parsed.path, qs

    def _send_headers(self, handler: server.BaseHTTPRequestHandler):
        handler.send_response(200)
        handler.send_header('Content-Type', 'application/json')
        handler.end_headers()


