# Process files and directories

The following rules traverse files from a directory or directories, and create output
based on multiple files.


For the following we assume your working directory is `<minion-install>/docs/files`

The used input set is the following sample of three Dockerfiles and some READMEs.

```
$ find . -type f
./sample_files/alpha/Dockerfile
./sample_files/alpha/README.md
./sample_files/beta/Dockerfile
./sample_files/gamma/Dockerfile
./sample_files/gamma/README.md
```

## Recipe: Scan for Dockerfile and README.md

The following rule set reads input directories and scan them for `Dockerfiles` and
`README.mds`. The latter file is not mandatory.
From each type of file a few data items are extracted. 
A file `image.csv` is created per directory.
Finally, a summary is collected to `images.csv`

Two rules use option `--dir` to input directories and not files.

```
rule IN --dir => file

rule file == "Dockerfile" => image-dir
    echo $file

rule file == "Dockerfile" => from-image
    sed -n "s/^FROM\(.*\)/\1/p" $file | xargs

rule file == "README.md" => description
    sed -n "s/^# \(.*\)/\1/p" $file | xargs

rule IN --dir with image-dir and [description or from-image] => image.csv
    echo "$value(image-dir), $value(description), $value(from-image)"

rule images.csv <= image.csv --target
    echo "Image, Title, FROM" > $OUT
    cat $(image.csv) >> $OUT

```

The rule set is given input directories as input, e.g.:

```
$ minion -qcr readme_and_dockerfile.rules use sample_files/*
Image, Title, FROM
sample_files/alpha/Dockerfile, Alpha is first, alpine
sample_files/beta/Dockerfile, , ubuntu
sample_files/gamma/Dockerfile, Gamma is third, busybox
```

## Recipe: Recursive scan for directories

The following set of rules is developed from the previous set so that
it scans recursively directories and creates "image.csv" for all 
directories which have "Dockerfile".

The beef is rule `rule IN --dir => IN` which inputs directory and outputs
(or rather aliases, as there are no commands)
the files from the directory.

```
rule IN --dir => file

rule file == "Dockerfile" => image-dir
    echo $file

rule file == "Dockerfile" => from-image
    sed -n "s/^FROM\(.*\)/\1/p" $file | xargs

rule file == "README.md" => description
    sed -n "s/^# \(.*\)/\1/p" $file | xargs

rule IN --dir with image-dir and [description or from-image] => image.csv
    echo "$value(image-dir), $value(description), $value(from-image)"

rule images.csv <= image.csv --target
    echo "Image, Title, FROM" > $OUT
    cat $(image.csv) >> $OUT
```

The rule set can be then given just the root directory of the sub directories to scan:

```
$ minion -qcr readme_and_dockerfile.rules use sample_files
Image, Title, FROM
sample_files/alpha/Dockerfile, Alpha is first, alpine
sample_files/beta/Dockerfile, , ubuntu
sample_files/gamma/Dockerfile, Gamma is third, busybox
```
