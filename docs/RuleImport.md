# Importing rules

Large rule sets can be split into multiple files and/or rules can be shared.
This is accomplished by `import`-statement.

An import seeks the imported rule file first from the same directory
where the importing rule file is, and then from built-in rule locations.

## Recipe: Import unpack.rules

Consider that you want to print the contents of some text files, which are inside zip-archive.
The following rule set uses the built-in rule set `unpack.rules` to accomplish this:

```
import unpack.rules IN, type

rule IN with type == "text/plain" => print
    cat $IN
```

The import statement `import unpack.rules IN, type` reads all rules from the file "import.rules",
but only exposes two names from it "IN" and "type". All other rules from imported file
are also in effect and used if required, but they do that transparently for the importing rule set.

The rule set above is roughly equal than writing the following rule set without imports:

```
rule IN => type
     file -b --mime-type $IN

rule IN with type == "application/zip" => IN
     unzip $IN -d $OUT

rule IN with type == "text/plain" => print
    cat $IN
```

Importing "unpack.rules" also unpacks tar and gzip archives in addition to zip archives.

## Recipe: Rename imports

Sometimes you may have conflicts that rules in different files use the same name
for different data. It is possible to rename imported names to work around this problem.
E.g. the following renames "type" into "file-type" as it uses name "type" for different purpose:

```
import unpack.rules IN, type as file-type

rule IN with file-type == "text/plain" => print
    cat $IN

rule IN with file-type => type
    echo "Type of $IN: $value(file-type)"
```
