# Rule troubleshooting

Writing rule is simple, but we all make mistakes (and there may be Minion bugs to sort out).
Below are some hints how to debug unexpected behavior.

## Malformatted minion rule file

You should get an error message, if minion rule files are malformatted.
The error should contain file name, line and column.

## Resolve a single file

When you have many failures with large number of files, it may be useful
to just ask Minion to try to resolve a single failed file and then observe
that. You can find the failed files from the log, see below.

Minion is asked to (try to) resolve a file with `file` sub command, e.g.:

```
$ minion file source/sample.zip.d/IN/sample.pdf.d/pdfid
```

## Ignoring changes

Usually if you edit a rule, e.g. to debug or fix a problem,
Minion reruns the command for all input files.
The same happens if you edit an input file.
This can be annoying if there is a large amount of files to process.

You can change this my argument `--ignore-changes`,
which blocks Minion for checking for changes in rules or input files
Missing files are still created, with new rules and/or inputs,
including output files which were not created due errors.


## Read logs

Debugging in Minion is all about reading the Minion logs.

Consider the following very simple rule set.
It resolves 'type' and 'strings' for all input files.
Zip files are opened to process the files.

```
rule IN => type
    file -b --mime-type $IN

rule IN => strings
	strings $IN

rule IN with type == "application/zip" => IN
	unzip $IN -d $OUT
```

Running Minion build with the rule set and input file 'a.txt' results something like this:

```
$ minion build minion.rules txt.zip
08:58:24 [x] txt.zip.d/strings
08:58:24 [x] txt.zip.d/type
strings txt.zip > txt.zip.d/strings
file -b --mime-type txt.zip > txt.zip.d/type
08:58:24 [x] txt.zip.d/IN
unzip txt.zip -d txt.zip.d/IN
08:58:24 [x] txt.zip.d/IN/a.txt.d/strings
08:58:24 [x] txt.zip.d/IN/a.txt.d/type
08:58:24 [x] txt.zip.d/IN/b.txt.d/type
08:58:24 [x] txt.zip.d/IN/b.txt.d/strings
Archive:  txt.zip
 extracting: txt.zip.d/IN/a.txt      
 extracting: txt.zip.d/IN/b.txt      
strings txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/strings
file -b --mime-type txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/type
file -b --mime-type txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/type
strings txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/strings
```

The lines with timestamp prefix are output from Minion.
Other lines are command stdout, or stderr, which is simply written to the terminal.
As Minion is processing rules in parallel, the log line order is a bit random.

Lines with `[x]` after timestamp indicate that a target is being processed.

## One thread

To have the log more readable, you should apply `--threads 1` option.
Now, Minion uses a single thread, so log reflects the temporal ordering of the events.

```
$ minion --threads 1 build minion.rules txt.zip
09:11:04 [x] txt.zip.d/strings
strings txt.zip > txt.zip.d/strings
09:11:04 [x] txt.zip.d/type
file -b --mime-type txt.zip > txt.zip.d/type
09:11:04 [x] txt.zip.d/IN
unzip txt.zip -d txt.zip.d/IN
Archive:  txt.zip
 extracting: txt.zip.d/IN/a.txt      
 extracting: txt.zip.d/IN/b.txt      
09:11:04 [x] txt.zip.d/IN/a.txt.d/strings
strings txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/strings
09:11:04 [x] txt.zip.d/IN/a.txt.d/type
file -b --mime-type txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/type
09:11:04 [x] txt.zip.d/IN/b.txt.d/strings
strings txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/strings
09:11:04 [x] txt.zip.d/IN/b.txt.d/type
file -b --mime-type txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/type
```

# Logging resolved targets

When re-running a rule set with same input file(s), Minion only runs a rule if there are changes
in the rule itself or in the input file.

So, re-running Minion without making any changes looks like this:

```
$ minion --threads 1 build minion.rules txt.zip
09:23:36 [/] txt.zip.d/type
09:23:36 [/] txt.zip.d/IN
09:23:36 [/] txt.zip.d/strings
09:23:36 [/] txt.zip.d/IN/a.txt.d/type
09:23:36 [/] txt.zip.d/IN/a.txt.d/strings
09:23:36 [/] txt.zip.d/IN/b.txt.d/type
09:23:36 [/] txt.zip.d/IN/b.txt.d/strings
```

Here marker `[/]` indicates that the target is already resolved.

If we make an edit to 'strings' rule, e.g.:

```
rule IN => strings
	echo $IN
```

Re-running with the change shows us that Minion only executes the changed rule:

```
$ minion --threads 1 build minion.rules txt.zip
09:25:30 [/] txt.zip.d/type
09:25:30 [/] txt.zip.d/IN
09:25:30 [x] txt.zip.d/strings
echo txt.zip > txt.zip.d/strings
09:25:30 [/] txt.zip.d/IN/a.txt.d/type
09:25:30 [x] txt.zip.d/IN/a.txt.d/strings
echo txt.zip.d/IN/a.txt > txt.zip.d/IN/a.txt.d/strings
09:25:30 [/] txt.zip.d/IN/b.txt.d/type
09:25:30 [x] txt.zip.d/IN/b.txt.d/strings
echo txt.zip.d/IN/b.txt > txt.zip.d/IN/b.txt.d/strings
```

## Error in rule command

A rule fails, if any of the commands return non-zero exit code.
The exit code is logged.
If rule writes error to stderr, that gets logged as well.
Any subsequent rules which would use output of the failed command are not ran.

```
$ minion --threads 1 build minion.rules txt.zip
...
13:43:01 [E] source/sample.zip.d/IN/sample.pdf.d/pdfid
13:43:01 Exit code 1 for pdf => pdfid # 35f42a93edac81b3 (rules/samples/pdf-pipeline.rules line 13)
...
Exception: Failed to create some files:
source/sample.zip.d/IN/sample.pdf.d/pdfid
  Exit code 1 for pdf => pdfid # 35f42a93edac81b3 (rules/samples/pdf-pipeline.rules line 13)
```

A summary of failed rules is printed at the end of the log.

## Jammed rule

When a rule jams, the minion processing jams.

FIXME: Timeout!

## Debug target

A target specific logging can be actived with command-line option `--debug-target`.
The option also automatically sets thread count to one so that log remains readable.

For example, consider the this rule from "pdf-pipeline":

```
rule IN with pdfid and sha256 and clamav => pdf.json
     cincan run -O "*.json" stedolan/jq '{"name": "$IN", "type": "pdf", "sha256": "$value(sha256)", "pdfid": .pdfid, "clamav": "$value(clamav)" }' $pdfid/*.json
```

The rule is debugged with following command line.
The rule log contains detailed log how the rule was invoked:

```
$ minion --debug-target "pdf.json" build samples/pdf-pipeline.rules sample.pdf
...
13:59:16 --- source/sample.zip.d/IN/sample.pdf.d/pdf.json
13:59:16 IN = source/sample.zip.d/IN/sample.pdf
13:59:16 pdfid = source/sample.zip.d/IN/sample.pdf.d/pdfid
13:59:16 sha256 = source/sample.zip.d/IN/sample.pdf.d/sha256
13:59:16 clamav = source/sample.zip.d/IN/sample.pdf.d/clamav
13:59:16 [x] source/sample.zip.d/IN/sample.pdf.d/pdf.json
cincan run -O "*.json" stedolan/jq '{"name": "source/sample.zip.d/IN/sample.pdf", "type": "pdf", "sha256": "67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac", "pdfid": .pdfid, "clamav": "Heuristics.PDF.ObfuscatedNameObject" }' source/sample.zip.d/IN/sample.pdf.d/pdfid/*.json > source/sample.zip.d/IN/sample.pdf.d/pdf.json
stedolan/jq: <= source/sample.zip.d/IN/sample.pdf
stedolan/jq: <= source/sample.zip.d/IN/sample.pdf.d/pdfid/pdfid_67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac.json
13:59:18 source/sample.zip.d/IN/sample.pdf.d/pdf.json took 1640 ms
...
```
