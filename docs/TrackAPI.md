# Track API

Minion has Web API for remote tracking the status of the execution and
to use breakpoints to debug problems.

NOTE: Currently the API is (even more than Minion itself) experimental, lacking
TLS and support for concurrent requests.
Please, use a reverse proxy to provide HTTPS connections if using over network.

## Features

Currently implemented features in the API

 * Authorization by API key string
 
 * Set and remove rule breakpoints
 
 * Break on all rules or at breakpoints
 
   * Before the rule evaluation starts
   
   * View input files
   
 * List loaded rules

 * List evaluated files
 
   * View input files  

## Start the API enabled

To enable the Track API, you must use `--api` option to provide 
optional interface name and the port to bind. If you omit the interface, then only
local interface is bind, e.g

    $ minion --api 8888 -r hello.rules use
    08:47:42 API at 127.0.0.1:8888 key=VP4QUpGsY71IahOqq4DK4RhlkzTsFfMZ
    08:47:42 Waiting for 'continue'
    08:47:42 Pause at start num=0

To change the bind interface, provide a value e.g. `192.168.1.101:8888`.
By default a random API key is used. Use `--api-key` to give a specific one
(any string will do).

## Call the API

When calling the API, you must provide the key with header `X-API-Key`, eg:

    $ wget --header "X-API-Key: VP4QUpGsY71IahOqq4DK4RhlkzTsFfMZ" "http://127.0.0.1:8000/events"

## API endpoints

| Method | URL                              | Description |
|--------|----------------------------------|-------------|
| GET    | `rules`                          | List rules |
| PUT    | `pause?value=state`              | Set automatic pause `state` value to `true` or `false` |
| GET    | `pause`                          | Get automatic pause state value
| GET    | `contexts/path`                  | Get evaluation context for file in `path` (relative to Minion root)
| GET    | `events`                         | Get all pending event `num`, wait if timeout
| GET    | `events/num`                     | Get pending event `num`, wait if timeout
| GET    | `events/latest`                  | Get latest event, wait if timeout
| PUT    | `events?action=continue`         | Continue all pending events` |
| PUT    | `events/num?action=continue`     | Continue event `num` |
| POST   | `breakpoints/path?line=num`      | Add breakpoint to rule in file `path` and line `num` |
| DELETE | `breakpoints/path?line=num`      | Delete breakpoint |

The method `GET` for endpoint `events` takes optional parameter `timeout=ms`, where
`ms` is milliseconds to wait for the event to happen. With this it is not required
to busy-poll new events.

For method `PUT` in endpoint `events` the value for flag `pause` can be
set in the query parameters, eg. `events?action=continue&pause=false`
to continue all break executions without pausing (unless a breakpoint).

Event numbers are increasing sequence starting with 0.

## Debugging session phases

A debugging session using API may have the general outline like this:

1. Start Minion with `--api` and perhaps with `--api-key`.

1. Minion starts, but breaks to wait for continue immediately

1. Use API call `POST breakpoints/path?line=num`
   to set breakpoints into desired rules

1. Resume execution with `PUT events/0?action=continue`, note that event number is 0.

1. Wait to hit a breakpoint, the call returns every 1000 ms,
   so run in a loop: `GET events/1?timeout=1000`.
   As you see we expect the next event number to be 1.

1. If we get back event data, a breakpoint is hit or the Minion has completed execution.

   * For a breakpoint, returned JSON has field `context`.

   * The event at the end of evaluation has field `stop`.

   It is possible to add and remove breakpoints at any time.

1. Continue the breakpoint as `PUT events/1?action=continue`.

1. Expect the next event to have number increased by one. Loop until done.
