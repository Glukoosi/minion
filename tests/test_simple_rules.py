from minion.file_facade import Facade
from minion.value import ValueName
from tests.test_context import establish_directory


def test_nested_scopes():
    test_dir = establish_directory('context', rule_file='nested_rules_and_values', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('info.txt'))
    assert f[0].value() == 'txt.zip.d/IN/a.txt: text/plain'
    assert f[1].value() == 'txt.zip.d/IN/b.txt: text/plain'
    assert f[2].value() == 'txt.zip: application/zip'
    assert len(f) == 3


def test_root_and_batch_rules():
    test_dir = establish_directory('context', rule_file='root_rule_and_batch_rule', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values()
    assert f[0].value() == 'ddfa3c372bb24cb9f12db42d34cb5273  txt.zip.d/IN/a.txt.d/text\n' +\
        '42d157236f093e1987d202a879566b4a  txt.zip.d/IN/b.txt.d/text'
    assert len(f) == 1


def test_pipe():
    test_dir = establish_directory('context', rule_file='pipe', sources='txt_2')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt'])
    f = facade.find_values(ValueName('piped'))
    assert f[0].value() == 'This is text A'
    assert f[1].value() == 'This is text B'
    assert len(f) == 2


def test_dir_rule():
    test_dir = establish_directory('context', rule_file='dir_rule', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('ls'))
    assert f[0].value() == 'txt.zip.d/IN'
    assert len(f) == 1


def test_only_dir_rule():
    test_dir = establish_directory('context', rule_file='only_print_dirs', sources='dir_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'ab_dir'])
    f = facade.find_values(ValueName('print'))
    assert f[0].value() == 'ab_dir'
    assert len(f) == 1

    test_dir = establish_directory('context', rule_file='only_print_dirs', sources='txt_1')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt'])
    f = facade.find_values(ValueName('print'))
    assert len(f) == 0


def test_dir_rule_sub_dirs():
    test_dir = establish_directory('context', rule_file='dir_rule', sources='zip_zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('ls'))
    assert f[0].value() == 'txt.zip.d/IN/sub.zip.d/IN'
    assert f[1].value() == 'txt.zip.d/IN'
    assert len(f) == 2


def test_dir_rule_in_dir():
    test_dir = establish_directory('context', rule_file='dir_rule', sources='dir_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'ab_dir'])
    f = facade.find_values(ValueName('ls'))
    assert f[0].value() == 'ab_dir'
    assert len(f) == 1


def test_not_dir_rule_in_dir():
    test_dir = establish_directory('context', rule_file='copy', sources='dir_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'ab_dir'])
    f = facade.find_values(ValueName('file.copy'))
    assert len(f) == 0


def test_dir_to_files():
    test_dir = establish_directory('context', rule_file='dir_to_files', sources='dir_dir_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'abcd_dir'])
    f = facade.find_values(ValueName('echo'))
    assert f[0].value() == 'abcd_dir/a.txt'
    assert f[1].value() == 'abcd_dir/ab_dir/c.txt'
    assert f[2].value() == 'abcd_dir/ab_dir/d.txt'
    assert f[3].value() == 'abcd_dir/b.txt'
    assert len(f) == 4


def test_bad_zip():
    test_dir = establish_directory('context', rule_file='type_strings_unzip', sources='zip_bad')
    facade = Facade.new(test_dir, sources=[test_dir / 'bad.zip'])
    f = facade.find_values(ValueName('strings'))
    assert f[0].as_string() == 'bad.zip.d/IN/bad.d_0.d/strings'
    assert f[1].as_string() == 'bad.zip.d/IN/bad.d_1.d/strings'
    assert f[2].as_string() == 'bad.zip.d/strings'
    assert len(f) == 3


def test_bad_input():
    test_dir = establish_directory('context', rule_file='dir_to_files', sources='bad_files')
    facade = Facade.new(test_dir, sources=[test_dir / 'ignore', test_dir / 'b.file', test_dir / 'include'])
    f = facade.find_values(ValueName('echo'))
    assert f[0].as_string() == 'b.file.d/echo'
    assert f[1].as_string() == 'include/e.file.d/echo'
    assert len(f) == 2


def test_build_with_fast_names():
    test_dir = establish_directory('context', rule_file='fast_name', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])

    facade.build_all()
    f = sorted(facade.manager.root_file.list_files(recurse=True))
    assert f[0].as_string() == 'minion.rules'
    assert f[1].as_string() == 'txt.zip'
    assert f[2].as_string() == 'txt.zip.d/IN/a.txt'
    assert f[3].as_string() == 'txt.zip.d/IN/a.txt.d/md'
    assert f[4].as_string() == 'txt.zip.d/IN/b.txt'
    assert f[5].as_string() == 'txt.zip.d/IN/b.txt.d/md'
    assert len(f) == 6


def test_dirs_as_scopes():
    test_dir = establish_directory('context', rule_file='dirs_as_scopes', sources='dir_dir_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'abcd_dir'])
    f = facade.find_values(ValueName('print'))
    assert f[0].as_string() == 'abcd_dir/ab_dir.d/print'
    assert f[1].as_string() == 'abcd_dir.d/print'
    assert f[0].value() == 'This is text C\nThis is text D'
    assert f[1].value() == 'This is text A\nThis is text B'
    assert len(f) == 2


def test_all_in():
    test_dir = establish_directory('context', rule_file='all_in', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('all_in'))
    assert f[0].value() == 'a.txt b.txt c.txt'
    assert len(f) == 1


def test_collect():
    test_dir = establish_directory('context', rule_file='collect', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('print'))
    assert f[0].value() == 'tag: a.txt\ntag: c.txt'


def test_collect_by_name():
    test_dir = establish_directory('context', rule_file='collect_by_name', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('print'))
    assert f[0].value() == 'tag: a.txt\ntag: c.txt'


def test_collect_non_root():
    test_dir = establish_directory('context', rule_file='collect_non_root', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('say_both'))
    assert f[0].value() == 'cfb1b4571285a792252cb8a3d862a71a  a.txt\na36addba33c01a5644462cb1061fd01bb566653e  a.txt'
    assert len(f) == 3


def test_collect_many():
    test_dir = establish_directory('context', rule_file='collect_many', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('say_both'))
    assert f[0].value() == 'a:a.txt\na:b.txt\na:c.txt\nb:a.txt\nb:b.txt\nb:c.txt'
    assert len(f) == 1


def test_collect_nested_deps_2():
    test_dir = establish_directory('context', rule_file='collect_nested_deps_2', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('print'))
    assert f[0].value() == 'a.txt from a.txt b.txt c.txt'
    assert f[1].value() == 'b.txt from a.txt b.txt c.txt'
    assert f[2].value() == 'c.txt from a.txt b.txt c.txt'


def test_collect_nested_deps_0():
    test_dir = establish_directory('context', rule_file='collect_nested_deps', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('all_names'))
    assert f[0].value() == 'a.txt b.txt c.txt'


def test_collect_nested_deps():
    test_dir = establish_directory('context', rule_file='collect_nested_deps', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('print'))
    assert f[0].value() == 'a.txt from a.txt b.txt c.txt'
    assert f[1].value() == 'b.txt from a.txt b.txt c.txt'
    assert f[2].value() == 'c.txt from a.txt b.txt c.txt'


def test_multivalue_backref():
    test_dir = establish_directory('context', rule_file='multivalue_with_backref', sources='zip_txt')
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip'])
    f = facade.find_values(ValueName('print'))
    assert f[0].relative_path.as_posix() == 'txt.zip.d/file/a.txt.d/file_deps.d/print'
    assert f[1].relative_path.as_posix() == 'txt.zip.d/file/b.txt.d/file_deps.d/print'
    assert f[0].value() == 'txt.zip.d/file/a.txt == txt.zip.d/file/a.txt.d/file_deps'
    assert f[1].value() == 'txt.zip.d/file/b.txt == txt.zip.d/file/b.txt.d/file_deps'
    assert len(f) == 2


def test_output_directory():
    test_dir = establish_directory('context', rule_file='output_directory', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('print.txt'))
    assert f[0].value() == 'tag: a.txt\ntag: c.txt'
    assert f[0].as_string() == 'subdir/print.txt'


def test_non_in_file_name():
    test_dir = establish_directory('context', rule_file='non_in_file_name', sources='txt_3')
    facade = Facade.new(test_dir, sources=[test_dir / 'a.txt', test_dir / 'b.txt', test_dir / 'c.txt'])
    f = facade.find_values(ValueName('echo'))
    assert f[0].value() == 'b.txt.d/non-in'
    assert len(f) == 1


def test_timeout():
    test_dir = establish_directory('context', rule_file='timeout')
    facade = Facade.new(test_dir)
    facade.manager.rules.rule_timeout = 0.1
    f = facade.get_context(test_dir / 'sleep')
    assert len(facade.scheduler.failed) == 1
    assert str(facade.scheduler.failed[0].error).startswith("Exit code 124")
