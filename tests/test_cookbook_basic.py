import pathlib
import shutil
import subprocess
from typing import Union


def copy_from_cookbook(directory: str, script: str = None) -> Union[str, pathlib.Path]:
    source_dir = pathlib.Path('docs') / directory
    root_dir = pathlib.Path('tests') / '_cookbook'
    shutil.rmtree(root_dir, ignore_errors=True)
    shutil.copytree(source_dir, root_dir, ignore=shutil.ignore_patterns('*.d'))
    if not script:
        return root_dir
    p = subprocess.run(['sh', script], stdout=subprocess.PIPE, cwd=root_dir)
    if p.returncode != 0:
        raise Exception(f"Exit code {p.returncode}")
    return p.stdout.decode('ascii')


def test_basic():
    out = copy_from_cookbook('basic', 'cat-file.sh')
    assert out == \
        'Hello, World!\n' \
        'hello.txt.d/cat-file\n' \
        'Hello, World!\n'


def test_rule_predicate():
    out = copy_from_cookbook('basic', 'rule-predicate.sh')
    assert sorted(out.split('\n')) == [
        '',
        'Hello, World!',
        'c3220f67afa12591543b25902cc2fc98  encoded.bin']


def test_summary():
    out = copy_from_cookbook('basic', 'summary.sh')
    assert out


def test_pipeline():
    out = copy_from_cookbook('basic', 'pipeline.sh')
    assert out


def test_type_and_unzip():
    out = copy_from_cookbook('basic', 'type-and-unzip.sh')
    assert out
