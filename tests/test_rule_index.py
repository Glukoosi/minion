from minion.file_facade import Facade
from tests.test_context import establish_directory


def test_root_and_batch_rules():
    file_list = "b.txt\ndir/d.txt\na.txt\n"
    test_dir = establish_directory('context', rule_file='indexed_rule', sources='zip_txt_5',
                                   files={'file.list': file_list})
    facade = Facade.new(test_dir, sources=[test_dir / 'txt.zip', test_dir / 'file.list'])
    f = facade.find_values()
    assert f[0].value() == 'file.list.d/file-name/a.txt: This is text A'
    assert f[1].value() == 'file.list.d/file-name/b.txt: This is text B'
    # assert f[2].value() == 'file.list.d/file-name/dir/d.txt: This is text D'
    assert len(f) == 2
