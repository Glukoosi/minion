from minion.builder import Builder
from minion.predicate import Predicates
from minion.file_facade import Facade
from minion.rules import CommandRule
from minion.value import INPUT_VALUE, ValueName
from tests.test_context import establish_directory


def test_looped_rules():
    unzip_rule = CommandRule.new(
        INPUT_VALUE, INPUT_VALUE, commands=['unzip -d $OUT $IN'],
        predicate=Predicates.simple_match(Predicates.IN_FILE_NAME, Predicates.string("*.zip")))
    txt_rule = CommandRule.new(
        INPUT_VALUE, 'txt', commands=['cat $IN'],
        predicate=Predicates.simple_match(Predicates.IN_FILE_NAME, Predicates.string("*.txt")))

    test_dir = establish_directory('context', sources='zip_txt')
    builder = Builder.new(test_dir, rules=[unzip_rule, txt_rule], sources=[test_dir / 'txt.zip'])
    facade = Facade(builder)
    f = facade.find_values(ValueName('txt'))
    assert f[0].as_string() == 'txt.zip.d/IN/a.txt.d/txt'
    assert f[1].as_string() == 'txt.zip.d/IN/b.txt.d/txt'
    assert len(f) == 2

    test_dir = establish_directory('context', sources='zip_zip_txt')
    builder = Builder.new(test_dir, rules=[unzip_rule, txt_rule], sources=[test_dir / 'txt.zip'])
    facade = Facade(builder)
    f = sorted(facade.find_values(ValueName('txt')))
    assert f[0].as_string() == 'txt.zip.d/IN/a.txt.d/txt'
    assert f[1].as_string() == 'txt.zip.d/IN/b.txt.d/txt'
    assert f[2].as_string() == 'txt.zip.d/IN/sub.zip.d/IN/c.txt.d/txt'
    assert f[3].as_string() == 'txt.zip.d/IN/sub.zip.d/IN/d.txt.d/txt'
    assert len(f) == 4


def test_looped_rules_2():
    unzip_rule = CommandRule.new(
        INPUT_VALUE, INPUT_VALUE, commands=['unzip -d $OUT $IN'],
        predicate=Predicates.simple_match(Predicates.IN_FILE_NAME, Predicates.string("*.zip")))
    txt_rule = CommandRule.new(
        INPUT_VALUE, 'txt', commands=['cat $IN'],
        predicate=Predicates.simple_match(Predicates.IN_FILE_NAME, Predicates.string("*.txt")))
    md_rule = CommandRule.new(
        INPUT_VALUE, 'md', commands=['md5sum $txt'],
        predicate=Predicates.ref('txt'))

    test_dir = establish_directory('context', sources='zip_zip_txt')
    builder = Builder.new(test_dir, rules=[unzip_rule, txt_rule, md_rule], sources=[test_dir / 'txt.zip'])
    facade = Facade(builder)
    f = sorted(facade.find_values(ValueName('md')))
    assert f[0].as_string() == 'txt.zip.d/IN/a.txt.d/md'
    assert f[1].as_string() == 'txt.zip.d/IN/b.txt.d/md'
    assert f[2].as_string() == 'txt.zip.d/IN/sub.zip.d/IN/c.txt.d/md'
    assert f[3].as_string() == 'txt.zip.d/IN/sub.zip.d/IN/d.txt.d/md'
    # assert f[4].as_string() == 'txt.zip.d/IN/sub.zip.d/md'
    # assert f[5].as_string() == 'txt.zip.d/md'
    assert len(f) == 4


def test_name_as_variable_name():
    a_rule = CommandRule.new(INPUT_VALUE, 'name', commands=['echo N.N.'])
    b_rule = CommandRule.new(INPUT_VALUE, 'b', commands=['echo $IN: $value(name)'], predicate=Predicates.IN_FILE_NAME)
    test_dir = establish_directory('context', files={'a_file': 'Afile'})
    builder = Builder.new(test_dir, rules=[a_rule, b_rule], sources=[test_dir / 'a_file'])
    facade = Facade(builder)
    f = facade.find_values(ValueName('b'))
    assert f[0].value() == 'a_file: N.N.'
    assert len(f) == 1
